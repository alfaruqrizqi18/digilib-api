-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22 Feb 2018 pada 16.00
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digilib`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_users`
--

CREATE TABLE `admin_users` (
  `id_admin` int(4) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `nama` text NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_users`
--

INSERT INTO `admin_users` (`id_admin`, `username`, `password`, `nama`, `level`) VALUES
(1, 'admin', '1', 'Higuita', 'Admin'),
(5, 'op1', '1', 'Rooney', 'Operator'),
(6, 'op2', '1', 'Ferdinand', 'Operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku_tables`
--

CREATE TABLE `buku_tables` (
  `id_buku` int(11) NOT NULL,
  `judul_buku` text NOT NULL,
  `pengarang` varchar(50) NOT NULL,
  `edisi` varchar(20) NOT NULL,
  `jurusan` varchar(20) NOT NULL,
  `no_panggil` varchar(20) NOT NULL,
  `isbn` varchar(15) NOT NULL,
  `subyek` text NOT NULL,
  `gmd` varchar(10) NOT NULL,
  `bahasa` varchar(10) NOT NULL,
  `penerbit` varchar(30) NOT NULL,
  `tahun_terbit` int(4) NOT NULL,
  `tempat_terbit` varchar(30) NOT NULL,
  `deskripsi_fisik` varchar(30) NOT NULL,
  `letak_buku` text NOT NULL,
  `thumbnail` text NOT NULL,
  `alamat_thumbnail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku_tables`
--

INSERT INTO `buku_tables` (`id_buku`, `judul_buku`, `pengarang`, `edisi`, `jurusan`, `no_panggil`, `isbn`, `subyek`, `gmd`, `bahasa`, `penerbit`, `tahun_terbit`, `tempat_terbit`, `deskripsi_fisik`, `letak_buku`, `thumbnail`, `alamat_thumbnail`) VALUES
(3, 'Pemrograman Web PHP dan MySQL untuk Sistem Informasi Perpustakaan', 'Eko Prasetyo', '-', 'Teknik Informatika', '005/PRA/p', '978-979-756-411', 'Programming, Website', 'Text', 'Indonesia', 'Graha Ilmu', 2008, 'Jogjakarta', '-', 'Tidak diketahui', 'http://192.168.43.184/digilib/thumbnail-buku/deb66b1b57ac103ceaf692caa335d0b4.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/deb66b1b57ac103ceaf692caa335d0b4.jpg'),
(4, 'Aplikasi Mobile Commerce Menggunakan PHP dan MySQL', 'Janner Sinarmata', '-', 'Teknik Informatika', '621.39 SIM a', '979-763-522-8', 'Komputer, Programming, Handphone', 'Text', 'Indonesia', 'Andi', 2006, 'Jogjakarta', 'x + 230 hlm,; 23 cm', '-', 'http://192.168.43.184/digilib/thumbnail-buku/eb15f6c428039611816b60e757f1f2bd.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/eb15f6c428039611816b60e757f1f2bd.jpg'),
(5, 'The Definitive Guide to MySQL 5', 'Michael Kofler - David Kramer', '-', 'Teknik Informatika', '005.75/85-22 Kof d', '9781590595350', 'Manajemen, Bahasa Inggris, Pencitraan', 'Text', 'English', 'Apress', 2005, '-', '784p.', '-', 'http://192.168.43.184/digilib/thumbnail-buku/032a4bbafff87cb1ae3444cd77f10bd4.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/032a4bbafff87cb1ae3444cd77f10bd4.jpg'),
(6, 'Akuntansi Perusahaan Manufaktur', 'Sigit Hermawan', '1', 'Akuntansi', '657.662/HER/a', '978-979-756-338', '-', 'Text', 'Indonesia', 'Graha Ilmu', 2008, 'Yogyakarta', '188 hlm, 1 jil.: 23 cm', '-', 'http://192.168.43.184/digilib/thumbnail-buku/a821a9657f5398e7b2225feeaafae1bb.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/a821a9657f5398e7b2225feeaafae1bb.jpg'),
(7, 'Sistem Informasi Akuntansi', 'James Hall', '4', 'Akuntansi', '657/HAL/s', '978-979-691-407', '-', 'Text', 'Indonesia', 'Salemba Empat', 2007, 'Jakarta', '2 jil.,21X 26 cm,636 hlm', 'Rak No.2', 'http://192.168.43.184/digilib/thumbnail-buku/3c36d14d14d5740422fdebad51b0b7c1.png', 'C:/xampp/htdocs/digilib/thumbnail-buku/3c36d14d14d5740422fdebad51b0b7c1.png'),
(8, 'Siklus Akuntansi Perusahaan', 'Hery', '1', 'Akuntansi', '657/HER/5', '978-979-756-256', '-', 'Text', 'Indonesia', 'Graha Ilmu', 2007, 'Jakarta', '210 hlm, 1 jil.: 26 cm', 'Rak No.3 Atas', 'http://192.168.43.184/digilib/thumbnail-buku/ac7b275a6cf965797fa46596878523b6.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/ac7b275a6cf965797fa46596878523b6.jpg'),
(9, 'Akuntansi Keuangan Menengah II', 'Hery', '-', 'Akuntansi', '657.004/HER/a', '978-979-061-092', '-', 'Text', 'Indonesia', 'Salemba Empat', 2009, 'Jakarta', '1 jil., 228 hlm.,19 x 26 cm', 'Rak No.3 Bawah Kiri', 'http://192.168.43.184/digilib/thumbnail-buku/263611fa728c4c690102d532c15297a6.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/263611fa728c4c690102d532c15297a6.jpg'),
(10, 'Industrial Machinery Repair', 'R. Keith Mobley - Ricky Smit', '-', 'PPM', 'R 621.8 SMI i', '0-7506-7621-3', 'Mesin, Perbaikan', 'Text', 'English', 'Butterowoth-Heinemann', 2003, 'Burlington', '-', '-', 'http://192.168.43.184/digilib/thumbnail-buku/default.png', ''),
(11, 'Reparasi Sistem Pelayanan Mesin Mobil', 'Daryanto', '-', 'PPM', '621.82/DAR/r', '-', 'Mesin, Reparasi, Pelayanan, Mobil', 'Text', 'Indonesia', 'Bumi Aksara', 2004, '-', '-', '-', 'http://192.168.43.184/digilib/thumbnail-buku/default.png', ''),
(12, 'Buku Cepat Seminggu Belajar Mysql dari Beginner Sampai Jadi Mahir', 'Muhammad Rizqi Alfaruq', '', 'Teknik Informatika', '', '', 'Database, Programming, Informatika', 'Text', 'Indonesia', 'Microsoft', 2011, 'US', '', '', 'http://192.168.43.184/digilib/thumbnail-buku/default.png', ''),
(13, 'Buku Cepat Seminggu Belajar Mysql dari Beginner Sampai Jadi Mahir II', 'Rizqi', '2', 'Teknik Informatika', '', '', '', 'Text', 'Indonesia', 'Microsoft', 2021, '', '', '', 'http://192.168.43.184/digilib/thumbnail-buku/default.png', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_tables`
--

CREATE TABLE `laporan_tables` (
  `id_laporan` int(11) NOT NULL,
  `judul_laporan` text NOT NULL,
  `tahun` int(4) NOT NULL,
  `jenis_laporan` varchar(20) NOT NULL,
  `alamat_file_laporan` text NOT NULL,
  `alamat_file_laporan_public` text NOT NULL,
  `nama_file` text NOT NULL,
  `jurusan` varchar(40) NOT NULL,
  `thumbnail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laporan_tables`
--

INSERT INTO `laporan_tables` (`id_laporan`, `judul_laporan`, `tahun`, `jenis_laporan`, `alamat_file_laporan`, `alamat_file_laporan_public`, `nama_file`, `jurusan`, `thumbnail`) VALUES
(13, 'Analisis dan Perancangan Sistem Informasi Pemasaran dan Persediaan Barang PT. Nycomed Amersham', 2011, 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/tppa/1a4fdf93b10c8ce0b1a4ac6ebd03e932.docx', 'http://192.168.43.184/digilib/laporan/tppa/1a4fdf93b10c8ce0b1a4ac6ebd03e932.docx', '1a4fdf93b10c8ce0b1a4ac6ebd03e932.docx', 'Teknik Informatika', 'http://192.168.43.184/digilib/thumbnail-laporan/ti.png'),
(14, 'Aplikasi Pemesanan Rental Mobil Hafa Yogyakarta Dengan Layanan Web dan WAP', 2010, 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/tppa/e8dabbe86a48f2e06841b589ff156b2b.docx', 'http://192.168.43.184/digilib/laporan/tppa/e8dabbe86a48f2e06841b589ff156b2b.docx', 'e8dabbe86a48f2e06841b589ff156b2b.docx', 'Teknik Informatika', 'http://192.168.43.184/digilib/thumbnail-laporan/ti.png'),
(15, 'Perancangan Perangkat Lunak Tender untuk Jasa Konsultan', 2012, 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/7e7fc9f9f5a577d937f8fd849ab8c56a.docx', 'http://192.168.43.184/digilib/laporan/proyek1/7e7fc9f9f5a577d937f8fd849ab8c56a.docx', '7e7fc9f9f5a577d937f8fd849ab8c56a.docx', 'Teknik Informatika', 'http://192.168.43.184/digilib/thumbnail-laporan/ti.png'),
(16, 'SET Analisa dan Perancangan Sistem Informasi Sumber Daya Manusia (SDM) PT. LEN', 2019, 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/a1c454c9a7fa50344386724a0cae2455.docx', 'http://192.168.43.184/digilib/laporan/proyek1/a1c454c9a7fa50344386724a0cae2455.docx', 'a1c454c9a7fa50344386724a0cae2455.docx', 'Teknik Informatika', 'http://192.168.43.184/digilib/thumbnail-laporan/ti.png'),
(17, 'Deteksi Muka Depan Manusia dari Sebuah Citra Berwarna dengan Template Matching', 2001, 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/ae5248d1ac09c6fe7840b24612c34ef9.docx', 'http://192.168.43.184/digilib/laporan/proyek1/ae5248d1ac09c6fe7840b24612c34ef9.docx', 'ae5248d1ac09c6fe7840b24612c34ef9.docx', 'Teknik Informatika', 'http://192.168.43.184/digilib/thumbnail-laporan/ti.png'),
(18, 'Perangkat Lunak Sistem Informasi Pegawai PT. Stannia Bineka Jasa', 2012, 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/5274087e08c384e242e9ed134697cd08.docx', 'http://192.168.43.184/digilib/laporan/proyek1/5274087e08c384e242e9ed134697cd08.docx', '5274087e08c384e242e9ed134697cd08.docx', 'Teknik Informatika', 'http://192.168.43.184/digilib/thumbnail-laporan/ti.png'),
(19, 'Perangkat Lunak Pemenuhan Kebutuhan Gizi pada Orang Sakit', 2012, 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/e314a22da2c6cdc1db82faf711683b49.docx', 'http://192.168.43.184/digilib/laporan/proyek1/e314a22da2c6cdc1db82faf711683b49.docx', 'e314a22da2c6cdc1db82faf711683b49.docx', 'Teknik Informatika', 'http://192.168.43.184/digilib/thumbnail-laporan/ti.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa_users`
--

CREATE TABLE `mahasiswa_users` (
  `id_mahasiswa` int(11) NOT NULL,
  `nim` int(9) NOT NULL,
  `password` text NOT NULL,
  `nama` text NOT NULL,
  `jurusan` varchar(40) NOT NULL,
  `token_android` text,
  `kesempatan_pinjam_buku` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa_users`
--

INSERT INTO `mahasiswa_users` (`id_mahasiswa`, `nim`, `password`, `nama`, `jurusan`, `token_android`, `kesempatan_pinjam_buku`) VALUES
(1, 15010086, '123', 'Muhammad Rizqi Alfaruq', 'Teknik Informatika', 'f8tMwPN9Re8:APA91bHg8C6Qn--viP3i3Xqo1ZLWB6KOPAWJThS8eF9F6h3aXDvqWUUpQO3dJnEzE9NVBgoeAwbD7TKYbCn0acwYY_3wuT8RO0xX5aVzTQ-Jjl_NtfnNTK_IBnmSJsZT4p5-Rz6Q7Y05', 5),
(2, 123, '123', 'Sumidi Jr.', 'PPM', 'f8QMvpsb7SQ:APA91bFMHOInsFRaEWFEhOlAhF6-34IzI1b8XeviL2109UBtGTjVHHBJN3Izxp_NH45IsBXyrtWQ35BvMxTMb92j4gLvlkCcWq2LnSRzuBrHmlEzS0heyO2BupxQ5rbLjfYUp47lHzqC', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifikasi_tables`
--

CREATE TABLE `notifikasi_tables` (
  `id_notifikasi` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `judul_notifikasi` text NOT NULL,
  `pesan_notifikasi` text NOT NULL,
  `token_android` text NOT NULL,
  `tgl_notifikasi` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemesanan_tables`
--

CREATE TABLE `pemesanan_tables` (
  `id_pemesanan` int(11) NOT NULL,
  `nomor_pemesanan_unik` text NOT NULL,
  `id_stok` int(11) NOT NULL,
  `nim` int(9) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `pemesanan_dimulai_pada_jam` varchar(9) NOT NULL,
  `pemesanan_hangus_pada_jam` varchar(9) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_buku_tables`
--

CREATE TABLE `stok_buku_tables` (
  `id_stok` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `unique_id_buku` varchar(50) NOT NULL,
  `is_available` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok_buku_tables`
--

INSERT INTO `stok_buku_tables` (`id_stok`, `id_buku`, `unique_id_buku`, `is_available`) VALUES
(1, 3, '33', 'true'),
(2, 3, '333', 'true'),
(3, 4, '4', 'true'),
(4, 4, '44', 'true'),
(5, 5, '5', 'true'),
(6, 5, '55', 'true'),
(7, 5, '555', 'true'),
(8, 5, '5555', 'true'),
(9, 6, '6', 'true'),
(10, 13, 'asdqeqwe', 'true'),
(11, 13, 'asdqwe2', 'true'),
(12, 10, 'cvsdqwe', 'true'),
(13, 10, '123asd1', 'true'),
(14, 13, 'asdca123', 'true'),
(15, 13, 'SDFXCV23', 'true'),
(16, 12, '123ASDC', 'true'),
(17, 12, 'QWE2333', 'true'),
(18, 13, 'AAS235G', 'true'),
(19, 11, 'GHJ234', 'true'),
(20, 11, '23446G', 'true'),
(21, 9, 'ASDDD2E23', 'true'),
(22, 9, '223FFX', 'true'),
(23, 9, 'GGBH23', 'true'),
(24, 9, '23122ZCC', 'true'),
(25, 8, 'SSA123', 'true'),
(26, 8, 'FGHTERT', 'true'),
(27, 8, '1234451XX', 'true'),
(28, 8, '546HHC', 'true'),
(29, 8, 'ZXCAQWE', 'true'),
(30, 8, 'CCVFSA', 'true'),
(31, 7, 'ASD23321', 'true'),
(32, 7, 'AA1289', 'true'),
(33, 7, 'BNMHJK', 'true'),
(34, 7, '213RT', 'true'),
(35, 7, 'ASDZXC', 'true');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_tables`
--

CREATE TABLE `transaksi_tables` (
  `id_transaksi` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tgl_pinjam_buku` date NOT NULL,
  `tgl_kembali_buku` date NOT NULL,
  `tgl_buku_dikembalikan` date NOT NULL,
  `nim` int(9) NOT NULL,
  `id_stok` int(11) NOT NULL,
  `status_transaksi` varchar(30) NOT NULL,
  `total_denda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_tables`
--

INSERT INTO `transaksi_tables` (`id_transaksi`, `tgl_transaksi`, `tgl_pinjam_buku`, `tgl_kembali_buku`, `tgl_buku_dikembalikan`, `nim`, `id_stok`, `status_transaksi`, `total_denda`) VALUES
(1, '2018-02-22', '2018-02-22', '2018-03-01', '2018-02-22', 15010086, 5, 'peminjaman_telah_berakhir', 0),
(2, '2018-02-22', '2018-02-22', '2018-03-01', '2018-02-22', 15010086, 6, 'peminjaman_telah_berakhir', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `buku_tables`
--
ALTER TABLE `buku_tables`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `laporan_tables`
--
ALTER TABLE `laporan_tables`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `mahasiswa_users`
--
ALTER TABLE `mahasiswa_users`
  ADD PRIMARY KEY (`id_mahasiswa`);

--
-- Indexes for table `notifikasi_tables`
--
ALTER TABLE `notifikasi_tables`
  ADD PRIMARY KEY (`id_notifikasi`);

--
-- Indexes for table `pemesanan_tables`
--
ALTER TABLE `pemesanan_tables`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indexes for table `stok_buku_tables`
--
ALTER TABLE `stok_buku_tables`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `transaksi_tables`
--
ALTER TABLE `transaksi_tables`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id_admin` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `buku_tables`
--
ALTER TABLE `buku_tables`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `laporan_tables`
--
ALTER TABLE `laporan_tables`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `mahasiswa_users`
--
ALTER TABLE `mahasiswa_users`
  MODIFY `id_mahasiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notifikasi_tables`
--
ALTER TABLE `notifikasi_tables`
  MODIFY `id_notifikasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pemesanan_tables`
--
ALTER TABLE `pemesanan_tables`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stok_buku_tables`
--
ALTER TABLE `stok_buku_tables`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `transaksi_tables`
--
ALTER TABLE `transaksi_tables`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
