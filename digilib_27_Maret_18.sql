-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27 Mar 2018 pada 05.11
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digilib`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_users`
--

CREATE TABLE `admin_users` (
  `id_admin` int(4) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `nama` text NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_users`
--

INSERT INTO `admin_users` (`id_admin`, `username`, `password`, `nama`, `level`) VALUES
(1, 'admin', '1', 'Higuita', 'Admin'),
(5, 'op1', '1', 'Rooney', 'Operator'),
(6, 'op2', '1', 'Ferdinand', 'Operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku_tables`
--

CREATE TABLE `buku_tables` (
  `id_buku` int(11) NOT NULL,
  `judul_buku` text NOT NULL,
  `pengarang` varchar(50) NOT NULL,
  `edisi` varchar(20) NOT NULL,
  `jurusan` varchar(20) NOT NULL,
  `no_panggil` varchar(20) NOT NULL,
  `isbn` varchar(15) NOT NULL,
  `subyek` text NOT NULL,
  `gmd` varchar(30) NOT NULL,
  `bahasa` varchar(10) NOT NULL,
  `penerbit` varchar(30) NOT NULL,
  `tahun_terbit` int(4) NOT NULL,
  `tempat_terbit` varchar(30) NOT NULL,
  `deskripsi_fisik` varchar(30) NOT NULL,
  `letak_buku` text NOT NULL,
  `thumbnail` text NOT NULL,
  `alamat_thumbnail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku_tables`
--

INSERT INTO `buku_tables` (`id_buku`, `judul_buku`, `pengarang`, `edisi`, `jurusan`, `no_panggil`, `isbn`, `subyek`, `gmd`, `bahasa`, `penerbit`, `tahun_terbit`, `tempat_terbit`, `deskripsi_fisik`, `letak_buku`, `thumbnail`, `alamat_thumbnail`) VALUES
(3, 'Pemrograman Web PHP dan MySQL untuk Sistem Informasi Perpustakaan', 'Eko Prasetyo', '-', 'Teknik Informatika', '005/PRA/p', '978-979-756-411', 'Programming, Website', 'Text', 'Indonesia', 'Graha Ilmu', 2008, 'Jogjakarta', '-', 'Tidak diketahui', 'thumbnail-buku/deb66b1b57ac103ceaf692caa335d0b4.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/deb66b1b57ac103ceaf692caa335d0b4.jpg'),
(4, 'Aplikasi Mobile Commerce Menggunakan PHP dan MySQL', 'Eko Prasetyo', '-', 'Teknik Informatika', '621.39 SIM a', '979-763-522-8', 'Komputer, Programming, Handphone', 'Digital Versatile Disc', 'Indonesia', 'Andi', 2006, 'Jogjakarta', 'x + 230 hlm,; 23 cm', '-', 'thumbnail-buku/eb15f6c428039611816b60e757f1f2bd.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/eb15f6c428039611816b60e757f1f2bd.jpg'),
(5, 'The Definitive Guide to MySQL 5', 'Michael Kofler - David Kramer', '-', 'Teknik Informatika', '005.75/85-22 Kof d', '9781590595350', 'Manajemen, Bahasa Inggris, Pencitraan', 'Text', 'English', 'Apress', 2005, '-', '784p.', '-', 'thumbnail-buku/032a4bbafff87cb1ae3444cd77f10bd4.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/032a4bbafff87cb1ae3444cd77f10bd4.jpg'),
(6, 'Akuntansi Perusahaan Manufaktur', 'Sigit Hermawan', '1', 'Akuntansi', '657.662/HER/a', '978-979-756-338', '-', 'Text', 'Indonesia', 'Graha Ilmu', 2008, 'Yogyakarta', '188 hlm, 1 jil.: 23 cm', '-', 'thumbnail-buku/a821a9657f5398e7b2225feeaafae1bb.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/a821a9657f5398e7b2225feeaafae1bb.jpg'),
(7, 'Sistem Informasi Akuntansi', 'James Hall', '4', 'Akuntansi', '657/HAL/s', '978-979-691-407', '-', 'Text', 'Indonesia', 'Salemba Empat', 2007, 'Jakarta', '2 jil.,21X 26 cm,636 hlm', 'Rak No.2', 'thumbnail-buku/3c36d14d14d5740422fdebad51b0b7c1.png', 'C:/xampp/htdocs/digilib/thumbnail-buku/3c36d14d14d5740422fdebad51b0b7c1.png'),
(8, 'Siklus Akuntansi Perusahaan', 'Hery', '1', 'Akuntansi', '657/HER/5', '978-979-756-256', '-', 'Text', 'Indonesia', 'Graha Ilmu', 2007, 'Jakarta', '210 hlm, 1 jil.: 26 cm', 'Rak No.3 Atas', 'thumbnail-buku/ac7b275a6cf965797fa46596878523b6.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/ac7b275a6cf965797fa46596878523b6.jpg'),
(9, 'Akuntansi Keuangan Menengah II', 'Hery', '-', 'Akuntansi', '657.004/HER/a', '978-979-061-092', '-', 'Text', 'Indonesia', 'Salemba Empat', 2009, 'Jakarta', '1 jil., 228 hlm.,19 x 26 cm', 'Rak No.3 Bawah Kiri', 'thumbnail-buku/263611fa728c4c690102d532c15297a6.jpg', 'C:/xampp/htdocs/digilib/thumbnail-buku/263611fa728c4c690102d532c15297a6.jpg'),
(10, 'Industrial Machinery Repair', 'R. Keith Mobley - Ricky Smit', '-', 'PPM', 'R 621.8 SMI i', '0-7506-7621-3', 'Mesin, Perbaikan', 'Text', 'English', 'Butterowoth-Heinemann', 2003, 'Burlington', '-', '-', 'thumbnail-buku/default.png', ''),
(11, 'Reparasi Sistem Pelayanan Mesin Mobil', 'Daryanto', '-', 'PPM', '621.82/DAR/r', '-', 'Mesin, Reparasi, Pelayanan, Mobil', 'Text', 'Indonesia', 'Bumi Aksara', 2004, '-', '-', '-', 'thumbnail-buku/default.png', ''),
(12, 'Buku Cepat Seminggu Belajar Mysql dari Beginner Sampai Jadi Mahir', 'Muhammad Rizqi Alfaruq', '', 'Teknik Informatika', '', '', 'Database, Programming, Informatika', 'Text', 'Indonesia', 'Microsoft', 2011, 'US', '', '', 'thumbnail-buku/default.png', ''),
(13, 'Buku Cepat Seminggu Belajar Mysql dari Beginner Sampai Jadi Mahir II', 'Rizqi', '2', 'Teknik Informatika', '123', '12123', 'asdasd', 'Digital Versatile Disc', 'Indonesia', 'Microsoft', 2021, 'Kediri', 'asdasd', 'asdasd', 'thumbnail-buku/default.png', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_tables`
--

CREATE TABLE `laporan_tables` (
  `id_laporan` int(11) NOT NULL,
  `judul_laporan` text NOT NULL,
  `tahun` int(4) NOT NULL,
  `abstrak` text NOT NULL,
  `kata_kunci` text NOT NULL,
  `jenis_laporan` varchar(20) NOT NULL,
  `alamat_file_laporan` text NOT NULL,
  `alamat_file_laporan_public` text NOT NULL,
  `nama_file` text NOT NULL,
  `jurusan` varchar(40) NOT NULL,
  `thumbnail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laporan_tables`
--

INSERT INTO `laporan_tables` (`id_laporan`, `judul_laporan`, `tahun`, `abstrak`, `kata_kunci`, `jenis_laporan`, `alamat_file_laporan`, `alamat_file_laporan_public`, `nama_file`, `jurusan`, `thumbnail`) VALUES
(13, 'Analisis dan Perancangan Sistem Informasi Pemasaran dan Persediaan Barang PT. Nycomed Amersham', 2011, '', '', 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/tppa/1a4fdf93b10c8ce0b1a4ac6ebd03e932.docx', 'laporan/tppa/1a4fdf93b10c8ce0b1a4ac6ebd03e932.docx', '1a4fdf93b10c8ce0b1a4ac6ebd03e932.docx', 'Teknik Informatika', 'thumbnail-laporan/ti.png'),
(14, 'Aplikasi Pemesanan Rental Mobil Hafa Yogyakarta Dengan Layanan Web dan WAP', 2010, '', '', 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/tppa/e8dabbe86a48f2e06841b589ff156b2b.docx', 'laporan/tppa/e8dabbe86a48f2e06841b589ff156b2b.docx', 'e8dabbe86a48f2e06841b589ff156b2b.docx', 'Teknik Informatika', 'thumbnail-laporan/ti.png'),
(15, 'Perancangan Perangkat Lunak Tender untuk Jasa Konsultan', 2012, '', '', 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/7e7fc9f9f5a577d937f8fd849ab8c56a.docx', 'laporan/proyek1/7e7fc9f9f5a577d937f8fd849ab8c56a.docx', '7e7fc9f9f5a577d937f8fd849ab8c56a.docx', 'Teknik Informatika', 'thumbnail-laporan/ti.png'),
(16, 'SET Analisa dan Perancangan Sistem Informasi Sumber Daya Manusia (SDM) PT. LEN', 2019, '', '', 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/a1c454c9a7fa50344386724a0cae2455.docx', 'laporan/proyek1/a1c454c9a7fa50344386724a0cae2455.docx', 'a1c454c9a7fa50344386724a0cae2455.docx', 'Teknik Informatika', 'thumbnail-laporan/ti.png'),
(17, 'Deteksi Muka Depan Manusia dari Sebuah Citra Berwarna dengan Template Matching', 2001, '', '', 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/ae5248d1ac09c6fe7840b24612c34ef9.docx', 'laporan/proyek1/ae5248d1ac09c6fe7840b24612c34ef9.docx', 'ae5248d1ac09c6fe7840b24612c34ef9.docx', 'Teknik Informatika', 'thumbnail-laporan/ti.png'),
(18, 'Perangkat Lunak Sistem Informasi Pegawai PT. Stannia Bineka Jasa', 2012, '', '', 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/5274087e08c384e242e9ed134697cd08.docx', 'laporan/proyek1/5274087e08c384e242e9ed134697cd08.docx', '5274087e08c384e242e9ed134697cd08.docx', 'Teknik Informatika', 'thumbnail-laporan/ti.png'),
(19, 'Perangkat Lunak Pemenuhan Kebutuhan Gizi pada Orang Sakit', 2012, '<p>Contoh Abstrak</p>\r\n', 'Perangkat Lunak, Gizi, Orang Sakit', 'Proyek 1', 'C:/xampp/htdocs/digilib/laporan/proyek1/e314a22da2c6cdc1db82faf711683b49.docx', 'laporan/proyek1/e314a22da2c6cdc1db82faf711683b49.docx', 'e314a22da2c6cdc1db82faf711683b49.docx', 'Teknik Informatika', 'thumbnail-laporan/ti.png'),
(20, 'Laporan Coba Aja sih', 2012, '<p>Di era globalisasi, seluruh sistem pelayanan menggunakan peralatan yang bersifat efisien waktu dan tenaga. Perkembangan teknologi informasi mendorong para konsumen untuk memanfaatkan sistem komputer sebagai salah satu alat atau <em>tools</em> untuk mendapatkan informasi. Salah satu contohnya adalah penerapan perpustakaan berbasis <em>digital</em>. Perpustakaan <em>digital</em> sama halnya seperti perpustakaan yang dikenal saat ini, hanya saja yang membedakan ialah cara pengguna untuk mendapatkan akses ke perpustakaan tersebut. Saat pengguna atau mahasiswa ingin melihat atau meminjam sebuah buku pada perpustakaan, hal pertama kali yang harus dilakukan adalah datang ke perpustakaan untuk melihat apakah buku tersebut tersedia di perpustakaan. Setelah itu jika memang tersedia, apakah stok buku tersebut masih ada untuk dipinjam. Hal tersebut dapat dipermudah dengan hadirnya perpustakaan digital. Dimana mahasiswa hanya perlu membuka aplikasi melalui smartphone dan semua informasi mengenai buku, laporan yang tersedia di perpustakaan dengan mudah diperoleh.</p>\r\n\r\n<p>Oleh karena itu, dengan memanfaatkan teknologi informasi yang semakin berkembang, dibangunlah Aplikasi Perpustakaan Berbasis Mobile yang diharapkan dapat membantu mahasiswa Politeknik Kediri dalam mencari informasi mengenai perpustakaan Politeknik Kediri.</p>\r\n', 'Aplikasi, Perpustakaan, Mobile', 'Prakerin', 'C:/xampp/htdocs/digilib/laporan/prakerin/f50267151b58b3d0fb0c55b251890333.pdf', 'laporan/prakerin/f50267151b58b3d0fb0c55b251890333.pdf', 'f50267151b58b3d0fb0c55b251890333.pdf', 'PPM', 'thumbnail-laporan/mesin.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_tables`
--

CREATE TABLE `log_tables` (
  `id_log` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `judul_log` varchar(60) NOT NULL,
  `isi_log` text NOT NULL,
  `id_stok` int(7) NOT NULL,
  `tgl_log` date NOT NULL,
  `type_log` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `log_tables`
--

INSERT INTO `log_tables` (`id_log`, `nim`, `judul_log`, `isi_log`, `id_stok`, `tgl_log`, `type_log`) VALUES
(1, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 2, '2018-03-21', 1),
(2, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 1, '2018-03-21', 1),
(3, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 3, '2018-03-21', 1),
(4, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 3, '2018-03-21', 1),
(5, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 2, '2018-03-21', 1),
(6, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 2, '2018-03-21', 1),
(7, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 3, '2018-03-21', 1),
(8, 15010086, 'Transaksi Peminjaman Buku', 'Kamu telah melakukan satu transaksi peminjaman buku dengan judul :', 3, '2018-03-21', 2),
(9, 15010086, 'Transaksi Peminjaman Buku', 'Kamu telah melakukan satu transaksi peminjaman buku dengan judul :', 2, '2018-03-21', 2),
(10, 15010086, 'Pengembalian Buku', 'Terima kasih, kamu telah mengembalikan buku dengan judul :', 2, '2018-03-21', 3),
(11, 15010086, 'Pengembalian Buku', 'Terima kasih, kamu telah mengembalikan buku dengan judul :', 3, '2018-03-21', 3),
(12, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 1, '2018-03-21', 1),
(13, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 2, '2018-03-21', 1),
(14, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 3, '2018-03-21', 1),
(15, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 3, '2018-03-21', 1),
(16, 15010086, 'Pemesanan Buku', 'Kamu telah memesan satu buah buku dengan judul :', 2, '2018-03-21', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa_users`
--

CREATE TABLE `mahasiswa_users` (
  `id_mahasiswa` int(11) NOT NULL,
  `nim` int(9) NOT NULL,
  `password` text NOT NULL,
  `nama` text NOT NULL,
  `jurusan` varchar(40) NOT NULL,
  `token_android` text,
  `kesempatan_pinjam_buku` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa_users`
--

INSERT INTO `mahasiswa_users` (`id_mahasiswa`, `nim`, `password`, `nama`, `jurusan`, `token_android`, `kesempatan_pinjam_buku`) VALUES
(1, 15010086, '123', 'Muhammad Rizqi Alfaruq', 'Teknik Informatika', 'dueZSfue5bU:APA91bHNOYP27Q1DkMojY82Mb9H1W-5mAkvuUU6zYFQhCyxT9JRzPwW6aqLnqVLVdAUhxsBM38GIiDy98PaSUl552JRknaiE1jVc403L4TISLR1kidoOZZZ2LbE2jqbk6BASC4_fiJQu', 3),
(2, 123, '1', 'Sumidi Jr.', 'PPM', 'dICe-IvV__Y:APA91bFb5x2pBnz1PQXvVhWCiSEXwmvbAoBD5j9Tt5VKF1p1tERPuh-Sbh5U0aPBWV-YImT4uRGl-XNtOsLdjzwS8qPLPx-ziWFgSEWxRrFCwUII8HOfSpBfavFbfZm8sVRfS1mUDyP9', 5),
(4, 1, '1', 'VIP Test 1', 'Akuntansi', 'fd9R9WtPYMY:APA91bEpkCc-IARq1qaqaqGUe1WLBRci0fX6bBQFpPckfDagWTsMrFdkBkCXyrJyytMns78wiESoRQ_CcrmaCB6fMTK4ryGlDzvFKqbAjmRePVbW8pu2Ch86ppFTvPDtW1gKcFg_8wlw', 3),
(5, 2, '2', 'VIP Test 2', 'PPM', 'epvCkSdnhzI:APA91bGxuuKp1NsDlvfhoEfhB7PqIC-Cu6UFQCEWuFcPdOcbIczIIq5prdE8HyGfqrW8PmaD_qMGXqHhxINwN5WZc80FMr-ddjYpzNXRLo5JrR9IA497_6rdhQNn8r6SrWtL3TSXoXOG', 5),
(6, 3, '3', 'VIP Test 3', 'Teknik Informatika', 'eCa-tmCczpA:APA91bH3bXrNM6_PxKJZqviOAbp558pr0taI38RyKvQWUf1kdK1_wtCgz2WkaBZDLYEB3lHelwhKUkcrIc-3v9hh4Vqt6vzILXwnHWmLSqgMCEAcIRhY-XHKqdMt4Fsw0vHqJTNB4c7m', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifikasi_tables`
--

CREATE TABLE `notifikasi_tables` (
  `id_notifikasi` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `judul_notifikasi` text NOT NULL,
  `pesan_notifikasi` text NOT NULL,
  `token_android` text NOT NULL,
  `tgl_notifikasi` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemesanan_tables`
--

CREATE TABLE `pemesanan_tables` (
  `id_pemesanan` int(11) NOT NULL,
  `nomor_pemesanan_unik` text NOT NULL,
  `id_stok` int(11) NOT NULL,
  `nim` int(9) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `pemesanan_dimulai_pada_jam` varchar(9) NOT NULL,
  `pemesanan_hangus_pada_jam` varchar(9) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemesanan_tables`
--

INSERT INTO `pemesanan_tables` (`id_pemesanan`, `nomor_pemesanan_unik`, `id_stok`, `nim`, `tgl_pemesanan`, `pemesanan_dimulai_pada_jam`, `pemesanan_hangus_pada_jam`, `status`) VALUES
(6, '5ab27ed70b21b', 2, 15010086, '2018-03-21', '22:48', '23:18', 'success'),
(7, '5ab27ef6a9e94', 3, 15010086, '2018-03-21', '22:49', '23:19', 'success'),
(11, '5ab2876274534', 3, 15010086, '2018-03-21', '23:25', '23:55', 'waiting'),
(12, '5ab289c3e9533', 2, 15010086, '2018-03-21', '23:35', '23:30', 'expired');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_buku_tables`
--

CREATE TABLE `stok_buku_tables` (
  `id_stok` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `unique_id_buku` varchar(50) NOT NULL,
  `is_available` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok_buku_tables`
--

INSERT INTO `stok_buku_tables` (`id_stok`, `id_buku`, `unique_id_buku`, `is_available`) VALUES
(1, 13, '123123123', 'true'),
(2, 13, 'asdasd', 'true'),
(3, 13, 'asdasdasdsa', 'false');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_tables`
--

CREATE TABLE `transaksi_tables` (
  `id_transaksi` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tgl_pinjam_buku` date NOT NULL,
  `tgl_kembali_buku` date NOT NULL,
  `tgl_buku_dikembalikan` date NOT NULL,
  `nim` int(9) NOT NULL,
  `id_stok` int(11) NOT NULL,
  `status_transaksi` varchar(30) NOT NULL,
  `total_denda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_tables`
--

INSERT INTO `transaksi_tables` (`id_transaksi`, `tgl_transaksi`, `tgl_pinjam_buku`, `tgl_kembali_buku`, `tgl_buku_dikembalikan`, `nim`, `id_stok`, `status_transaksi`, `total_denda`) VALUES
(1, '2018-03-21', '2018-03-21', '2018-03-28', '2018-03-21', 15010086, 3, 'peminjaman_telah_berakhir', 0),
(2, '2018-03-21', '2018-03-21', '2018-03-28', '2018-03-21', 15010086, 2, 'peminjaman_telah_berakhir', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `buku_tables`
--
ALTER TABLE `buku_tables`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `laporan_tables`
--
ALTER TABLE `laporan_tables`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `log_tables`
--
ALTER TABLE `log_tables`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `mahasiswa_users`
--
ALTER TABLE `mahasiswa_users`
  ADD PRIMARY KEY (`id_mahasiswa`);

--
-- Indexes for table `notifikasi_tables`
--
ALTER TABLE `notifikasi_tables`
  ADD PRIMARY KEY (`id_notifikasi`);

--
-- Indexes for table `pemesanan_tables`
--
ALTER TABLE `pemesanan_tables`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indexes for table `stok_buku_tables`
--
ALTER TABLE `stok_buku_tables`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `transaksi_tables`
--
ALTER TABLE `transaksi_tables`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id_admin` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `buku_tables`
--
ALTER TABLE `buku_tables`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `laporan_tables`
--
ALTER TABLE `laporan_tables`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `log_tables`
--
ALTER TABLE `log_tables`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `mahasiswa_users`
--
ALTER TABLE `mahasiswa_users`
  MODIFY `id_mahasiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `notifikasi_tables`
--
ALTER TABLE `notifikasi_tables`
  MODIFY `id_notifikasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pemesanan_tables`
--
ALTER TABLE `pemesanan_tables`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `stok_buku_tables`
--
ALTER TABLE `stok_buku_tables`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaksi_tables`
--
ALTER TABLE `transaksi_tables`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
