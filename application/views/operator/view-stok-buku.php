<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">
                    <img class="light-blue" alt="" height="75px;">
                    <?php foreach ($data_stok_buku as $data) {  ?>
                    <span class="card-title"><?php echo $data['judul_buku'] ?></span>
                    <?php } ?>
                </div>
                <div class="card-content">
                    <a href="operator/data-stok-buku" class="waves-effect waves-light light-blue btn">
                        <i class="material-icons">replay</i>
                    </a>
                    <br><br>
                    <table id="example" class="display responsive-table datatable-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>No. Buku</th>
                                <th>Ketersediaan</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>No. Buku</th>
                                <th>Ketersediaan</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php 
                            $nomor = 1;
                            foreach ($data_stok_buku as $data) { 
                            ?>
                            <tr>
                                <td><?php echo $nomor; ?></td>
                                <td><?php echo $data['judul_buku']; ?></td>
                                <td><?php echo $data['unique_id_buku']; ?></td>
                                <td>
                                    <?php if ($data['is_available'] == 'true'): ?>
                                        <div class="chip waves-effect waves-blue blue" style="color: white;">Tersedia</div>
                                    <?php elseif ($data['is_available'] == 'false'):?>
                                        <div class="chip waves-effect waves-red red" style="color: white;">Tidak tersedia</div>
                                    <?php endif ?>
                                </td>
                                <td>
                                    <a class='button btn red' href='<?php echo base_url('operator/delete_stok_buku/'.$data['id_buku'].'/'.$data['id_stok']);?>'>Hapus</a>
                                </td>
                            </tr>
                            <?php 
                            $nomor++;  
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true,
            // columnDefs: [
            // { width: 300, targets: 1 },
            // { width: 100, targets: 2 },
            // { width: 90, targets: 5 },
            // { width: 50, targets: 6 }
            // ],
            language: {
                searchPlaceholder: 'Search records',
                sSearch: '',
                sLengthMenu: 'Show _MENU_',
                sLength: 'dataTables_length',
                oPaginate: {
                    sFirst: '<i class="material-icons">chevron_left</i>',
                    sPrevious: '<i class="material-icons">chevron_left</i>',
                    sNext: '<i class="material-icons">chevron_right</i>',
                    sLast: '<i class="material-icons">chevron_right</i>' 
                }
            }
        });
        $('.dataTables_length select').addClass('browser-default');
    });
</script>
</body>
</html>