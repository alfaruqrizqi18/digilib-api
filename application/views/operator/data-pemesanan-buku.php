<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">
                    <img class="light-blue" alt="" height="75px;">
                    <span class="card-title">Buku - Data Master Pemesanan Buku</span>
                </div>
                <div class="card-content">
                    <?php if ($this->session->flashdata('error') != null): ?>
                        <div class="card-panel red ">
                            <span class="white-text">
                                <?php echo $this->session->flashdata('error'); ?>
                            </span>
                        </div>
                    <?php endif ?>
                    <?php if ($this->session->flashdata('success') != null): ?>
                        <div class="card-panel green ">
                            <span class="white-text">
                                <?php echo $this->session->flashdata('success'); ?>
                            </span>
                        </div>
                    <?php endif ?>
                    <table id="example" class="display responsive-table datatable-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No. Pemesanan</th>
                                <th>No. Buku</th>
                                <th>Judul Buku</th>
                                <th>Mahasiswa</th>
                                <th>Tanggal</th>
                                <th>Waktu</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>No. Pemesanan</th>
                                <th>No. Buku</th>
                                <th>Judul Buku</th>
                                <th>Mahasiswa</th>
                                <th>Tanggal</th>
                                <th>Waktu</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php 
                            $nomor = 1;
                            foreach ($data_pemesanan as $data) { ?>
                            <tr>
                                <td><?php echo $nomor; ?></td>
                                <td><b>DL#</b><?php echo $data['nomor_pemesanan_unik']; ?></td>
                                <td><?php echo $data['unique_id_buku']; ?></td>
                                <td><?php echo $data['judul_buku']; ?></td>
                                <td><?php echo $data['nama']; ?></td>
                                <td><?php echo date_format(date_create($data['tgl_pemesanan']), "d M Y"); ?></td>
                                <td>
                                    <b>
                                <?php echo $data['pemesanan_dimulai_pada_jam']." - ". $data['pemesanan_hangus_pada_jam']; ?>
                                    </b>
                                </td>

                                
                                <?php if ($data['status'] == 'waiting'): ?>
                                    <td>
                                        <div class="chip waves-effect waves-blue blue" style="color: white;">Menunggu</div>
                                    </td>
                                    <td>
                                        <a class='dropdown-button btn blue' href='#' data-activates='dropdown-<?php echo $nomor; ?>'>Actions</a>
                                        <!-- Dropdown Structure -->
                                        <ul id='dropdown-<?php echo $nomor; ?>' class='dropdown-content'>
                                            <li><a href="<?php echo base_url('operator/add_peminjaman_from_pemesanan/'.$data['id_pemesanan'].'/'.$data['nim'].'/'.$data['id_stok']) ?>">Lanjut Peminjaman</a></li>
                                            <li class="divider"></li>
                                        </ul>
                                    </td>
                                <?php elseif ($data['status'] == 'expired'): ?>
                                    <td>
                                        <div class="chip waves-effect waves-orange orange" style="color: white;">Kadaluwarsa</div>
                                    </td>

                                    <td>
                                        
                                    </td>

                                <?php elseif ($data['status'] == 'success'): ?>
                                    <td>
                                        <div class="chip waves-effect waves-green green" style="color: white;">Sukses</div>
                                    </td>

                                    <td>
                                        
                                    </td>

                                <?php elseif ($data['status'] == 'failed'): ?>
                                    <td>
                                        <div class="chip waves-effect waves-red red" style="color: white;">Gagal</div>
                                    </td>

                                    <td>
                                        
                                    </td>

                                <?php endif ?>

                            </tr>
                            <?php $nomor++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true,
            // columnDefs: [
            // { width: 110, targets: 1 },
            // { width: 200, targets: 2 },
            // { width: 90, targets: 3 },
            // { width: 90, targets: 4 },
            // { width: 90, targets: 5 },
            // { width: 30, targets: 6 }
            // ],
            language: {
                searchPlaceholder: 'Search records',
                sSearch: '',
                sLengthMenu: 'Show _MENU_',
                sLength: 'dataTables_length',
                oPaginate: {
                    sFirst: '<i class="material-icons">chevron_left</i>',
                    sPrevious: '<i class="material-icons">chevron_left</i>',
                    sNext: '<i class="material-icons">chevron_right</i>',
                    sLast: '<i class="material-icons">chevron_right</i>' 
                }
            }
        });
        $('.dataTables_length select').addClass('browser-default');
    });
</script>
</body>
</html>