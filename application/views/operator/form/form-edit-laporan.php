<script src="https://cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-content">
                      <?php if ($this->session->flashdata('error') != null): ?>
                        <div class="card-panel red ">
                          <span class="white-text">
                            <?php echo $this->session->flashdata('error'); ?>
                          </span>
                        </div>
                      <?php endif ?>
                    <div class="row">
                    <span class="card-title">Form Edit Laporan</span><br>
                        <?php foreach ($data_laporan as $data) { ?>
                        <form class="col s12" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="judul" type="text" required class="validate" name="judul" 
                                    value="<?php echo $data['judul_laporan']; ?>">
                                    <label for="judul">Judul Laporan</label>
                                </div>
                                <div class="input-field col s4">
                                    <input id="tahun" type="number" required class="validate" name="tahun" 
                                    value="<?php echo $data['tahun'] ?>">
                                    <label for="tahun">Tahun</label>
                                </div>
                                <div class="input-field col s4">
                                    <select name="jenis" required>
                                        <option value="" disabled>Choose your option</option>
                                        <option value="<?php echo $data['jenis_laporan']; ?>" selected>Saat ini : <?php echo $data['jenis_laporan'];; ?></option>
                                        <option value="Proyek 1">Proyek 1</option>
                                        <option value="Proyek 2">Proyek 2</option>
                                        <option value="Prakerin">Prakerin</option>
                                        <option value="TPPA">TPPA</option>
                                        <option value="Proyek Akhir">Proyek Akhir</option>
                                    </select>
                                    <label>Jenis Laporan</label>
                                </div>
                                <div class="input-field col s4">
                                    <select name="jurusan" required>
                                        <option value="" disabled>Choose your option</option>
                                        <option value="<?php echo $data['jurusan']; ?>" selected>Saat ini : <?php echo $data['jurusan']; ?></option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="PPM">PPM</option>
                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                    </select>
                                    <label>Jurusan</label>
                                </div>

                                <div class="input-field col s12">
                                <label>Abstrak</label><br>
                                    <textarea name="abstrak" id="abstrak" required="" class="validate" placeholder=""><?php echo $data['abstrak']; ?></textarea>
                                    <script>
                                        CKEDITOR.replace("abstrak");
                                    </script>
                                </div>
                                <br>
                                <div class="input-field col s12">
                                    <input id="kata_kunci" type="text" required class="validate" name="kata_kunci" value="<?php echo $data['kata_kunci'] ?>">
                                    <label for="kata_kunci">Kata Kunci</label>
                                </div>

                                <div class="file-field input-field col s12">
                                    <i class="material-icons prefix">subtitles</i>
                                    <textarea id="icon_prefix2" class="materialize-textarea" readonly 
                                    style="height: 21.2px;" name="nama_file"><?php echo $data['nama_file']; ?></textarea>
                                    <label for="icon_prefix2" class="">Berkas yang sudah ada</label>
                                    <input id="judul" type="hidden" class="validate" name="berkas_lama"
                                    value="<?php echo $data['alamat_file_laporan']; ?>">
                                    <button class="waves-effect waves-light btn teal lighten-1" id="btn-perbarui" type="button" onclick="show()">Perbarui berkas</button>
                                    <!-- -->
                                    <div class="btn teal lighten-1" id="berkas-baru">
                                        <span>Pilih berkas</span>
                                        <input type="file" name="berkas" accept=".doc, .docx, .pdf" >
                                    </div>
                                    <div class="file-path-wrapper" id="text-berkas-baru">
                                        <input class="file-path validate" type="text" placeholder="Unggah berkas dengan format (.pdf, .doc, .docx)">
                                    </div>
                                </div>
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light green btn" type="submit" name="btn_submit">Simpan</button>
                                    <a href="operator/data-laporan" class="waves-effect waves-light pink btn" type="submit">Kembali</a>
                                </div>
                            </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script type="text/javascript">
    $('#berkas-baru').hide();
    $('#text-berkas-baru').hide();
    function show() {
        $('#berkas-baru').show();
        $('#text-berkas-baru').show();
        $('#btn-perbarui').hide();
    }
</script>
</body>
</html>