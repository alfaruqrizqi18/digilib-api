<main class="mn-inner">
    <div class="row">
        <?php foreach ($data_buku as $data) { ?>
        <div class="col s12 m4 l12">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                    <span class="card-title">Edit Buku</span><br>
                        <form class="col s12" method="post" enctype="multipart/form-data">
                            <?php foreach ($data_buku as $data) { ?>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="judul" type="text" name="judul" required class="validate" value="<?php echo $data['judul_buku'] ?>">
                                    <label for="judul">Judul Buku</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="pengarang" type="text" name="pengarang" required class="validate" value="<?php echo $data['pengarang'] ?>">
                                    <label for="pengarang">Pengarang</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="edisi" type="text" name="edisi" required class="validate" value="<?php echo $data['edisi'] ?>">
                                    <label for="edisi">Edisi</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="no_panggil" type="text" name="no_panggil" required class="validate" value="<?php echo $data['no_panggil'] ?>">
                                    <label for="no_panggil">No. Panggil</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="isbn" type="text" name="isbn" required class="validate" value="<?php echo $data['isbn'] ?>">
                                    <label for="isbn">ISBN</label>
                                </div>
                                <div class="input-field col s3">
                                    <select name="gmd" required>
                                        <option value="" disabled>Choose your option</option>
                                        <option value="<?php echo $data['gmd'] ?>" selected>Saat ini : <?php echo $data['gmd'] ?></option>
                                        <option value="Text">Text</option>
                                        <option value="Art Original">Art Original</option>
                                        <option value="Chart">Chart</option>
                                        <option value="Computer Software">Computer Software</option>
                                        <option value="Diorama">Diorama</option>
                                        <option value="Filmstrip">Filmstrip</option>
                                        <option value="Flash Card">Flash Card</option>
                                        <option value="Game">Game</option>
                                        <option value="Globe">Globe</option>
                                        <option value="Kit">Kit</option>
                                        <option value="Map">Map</option>
                                        <option value="Microform">Microform</option>
                                        <option value="Manuscript">Manuscript</option>
                                        <option value="Model">Model</option>
                                        <option value="Motion Picture">Motion Picture</option>
                                        <option value="Microscope Slide">Microscope Slide</option>
                                        <option value="Music">Music</option>
                                        <option value="Picture">Picture</option>
                                        <option value="Realia">Realia</option>
                                        <option value="Slide">Slide</option>
                                        <option value="Sound Recording">Sound Recording</option>
                                        <option value="Technical Drawing">Technical Drawing</option>
                                        <option value="Transparency">Transparency</option>
                                        <option value="Video Recording">Video Recording</option>
                                        <option value="Equipment">Equipment</option>
                                        <option value="Computer File">Computer File</option>
                                        <option value="Cartographic Material">Cartographic Material</option>
                                        <option value="CD-ROM">CD-ROM</option>
                                        <option value="Multimedia">Multimedia</option>
                                        <option value="Electronic Resource">Electronic Resource</option>
                                        <option value="Digital Versatile Disc">Digital Versatile Disc</option>
                                    </select>
                                    <label>GMD</label>
                                </div>
                                <div class="input-field col s3">
                                    <select name="bahasa" required>
                                        <option value="" disabled >Choose your option</option>
                                        <option value="<?php echo $data['bahasa'] ?>" selected>Saat ini : <?php echo $data['bahasa'] ?></option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="English">English</option>
                                    </select>
                                    <label for="bahasa">Bahasa</label>
                                </div>
                                <div class="input-field col s3">
                                    <select name="jurusan" required>
                                        <option value="" disabled>Choose your option</option>
                                        <option value="<?php echo $data['jurusan'] ?>" selected><?php echo $data['jurusan'] ?></option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="PPM">PPM</option>
                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                    </select>
                                    <label>Jurusan</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="penerbit" name="penerbit" type="text" required class="validate" value="<?php echo $data['penerbit'] ?>">
                                    <label for="penerbit">Penerbit</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="tahun" name="tahun" type="number" required class="validate" value="<?php echo $data['tahun_terbit'] ?>">
                                    <label for="tahun">Tahun Terbit</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="tempat" name="tempat" type="text" required class="validate" value="<?php echo $data['tempat_terbit'] ?>">
                                    <label for="tempat">Tempat Terbit</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" name="subyek" class="materialize-textarea validate" required 
                                    style="height: 21.2px;"><?php echo $data['subyek'] ?></textarea>
                                    <label>Subyek</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" name="deskripsi" class="materialize-textarea validate" required 
                                    style="height: 21.2px;"><?php echo $data['deskripsi_fisik'] ?></textarea>
                                    <label>Deskripsi Fisik</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" class="materialize-textarea validate" required 
                                    style="height: 21.2px;" name="letak"><?php echo $data['letak_buku'] ?></textarea>
                                    <label>Letak Buku</label>
                                </div>
                                <div class="col s3 m4 l3">
                                    <div class="card-content center-align">
                                        <img src="<?php echo $data['thumbnail'] ?>" class="responsive-img" alt="">
                                        <input type="hidden" name="thumbnail_lama" value="<?php echo $data['thumbnail'] ?>">
                                    </div>
                                </div>
                                <div class="file-field input-field col s12">
                                    <div class="btn teal lighten-1">
                                        <span>Gambar Baru</span>
                                        <input type="file" name="thumbnail_baru" accept=".png, .jpg, .jpeg">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Unggah thumbnail dengan format (.png, .jpg, .jpeg)">
                                    </div>
                                </div>
                                <div class="input-field col s12">
                                    <button type="submit" class="waves-effect waves-light orange btn">Edit</button>
                                    <a href="operator/data-buku/" class="waves-effect waves-light pink btn" type="button">Kembali</a>
                                </div>
                            </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
</body>
</html>