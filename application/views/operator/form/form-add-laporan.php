<script src="https://cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-content">
                      <?php if ($this->session->flashdata('error') != null): ?>
                        <div class="card-panel red ">
                          <span class="white-text">
                            <?php echo $this->session->flashdata('error'); ?>
                          </span>
                        </div>
                      <?php endif ?>
                    <div class="row">
                    <span class="card-title">Form Tambah Laporan</span><br>
                        <form class="col s12" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="judul" type="text" required class="validate" name="judul">
                                    <label for="judul">Judul Laporan</label>
                                </div>
                                <div class="input-field col s4">
                                    <input id="tahun" type="number" required class="validate" name="tahun">
                                    <label for="tahun">Tahun</label>
                                </div>
                                <div class="input-field col s4">
                                    <select name="jenis" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="Proyek 1">Proyek 1</option>
                                        <option value="Proyek 2">Proyek 2</option>
                                        <option value="Prakerin">Prakerin</option>
                                        <option value="TPPA">TPPA</option>
                                        <option value="Proyek Akhir">Proyek Akhir</option>
                                    </select>
                                    <label>Jenis Laporan</label>
                                </div>
                                <div class="input-field col s4">
                                    <select name="jurusan" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="PPM">PPM</option>
                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                    </select>
                                    <label>Jurusan</label>
                                </div>
                                <div class="input-field col s12">
                                <label>Abstrak</label><br>
                                    <textarea name="abstrak" id="abstrak" required="" class="validate" placeholder=""></textarea>
                                    <script>
                                        CKEDITOR.replace("abstrak");
                                    </script>
                                </div>
                                <br>
                                <div class="input-field col s12">
                                    <input id="kata_kunci" type="text" required class="validate" name="kata_kunci">
                                    <label for="kata_kunci">Kata Kunci</label>
                                </div>
                                <div class="file-field input-field col s12">
                                    <div class="btn teal lighten-1">
                                        <span>Pilih berkas</span>
                                        <input type="file" name="berkas" accept=".doc, .docx, .pdf" required="">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Unggah berkas dengan format (.pdf, .doc, .docx)">
                                    </div>
                                </div>
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light green btn" type="submit" name="btn_submit" value="1">Simpan</button>
                                    <button class="waves-effect waves-light orange btn" type="submit" name="btn_submit" value="2">Simpan Tambah Baru</button>
                                    <a href="operator/data-laporan" class="waves-effect waves-light pink btn" type="button">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
</body>
</html>