<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-content">
                    <?php if ($this->session->flashdata('error') != null): ?>
                        <div class="card-panel red ">
                            <span class="white-text">
                                <?php echo $this->session->flashdata('error'); ?>
                            </span>
                        </div>
                    <?php endif ?>
                    <div class="row">
                    <span class="card-title">Form Tambah Buku</span><br>
                        <form class="col s12" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="judul" type="text" required class="validate" name="judul">
                                    <label for="judul">Judul Buku</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="pengarang" type="text" required class="validate" name="pengarang">
                                    <label for="pengarang">Pengarang</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="edisi" type="text" class="validate" name="edisi">
                                    <label for="edisi">Edisi</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="no_panggil" type="text" class="validate" name="no_panggil">
                                    <label for="no_panggil">No. Panggil</label>
                                </div>
                                <div class="input-field col s4">
                                    <input id="isbn" type="text" class="validate" name="isbn">
                                    <label for="isbn">ISBN</label>
                                </div>
                                <div class="input-field col s3">
                                    <select name="gmd" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="Text">Text</option>
                                        <option value="Art Original">Art Original</option>
                                        <option value="Chart">Chart</option>
                                        <option value="Computer Software">Computer Software</option>
                                        <option value="Diorama">Diorama</option>
                                        <option value="Filmstrip">Filmstrip</option>
                                        <option value="Flash Card">Flash Card</option>
                                        <option value="Game">Game</option>
                                        <option value="Globe">Globe</option>
                                        <option value="Kit">Kit</option>
                                        <option value="Map">Map</option>
                                        <option value="Microform">Microform</option>
                                        <option value="Manuscript">Manuscript</option>
                                        <option value="Model">Model</option>
                                        <option value="Motion Picture">Motion Picture</option>
                                        <option value="Microscope Slide">Microscope Slide</option>
                                        <option value="Music">Music</option>
                                        <option value="Picture">Picture</option>
                                        <option value="Realia">Realia</option>
                                        <option value="Slide">Slide</option>
                                        <option value="Sound Recording">Sound Recording</option>
                                        <option value="Technical Drawing">Technical Drawing</option>
                                        <option value="Transparency">Transparency</option>
                                        <option value="Video Recording">Video Recording</option>
                                        <option value="Equipment">Equipment</option>
                                        <option value="Computer File">Computer File</option>
                                        <option value="Cartographic Material">Cartographic Material</option>
                                        <option value="CD-ROM">CD-ROM</option>
                                        <option value="Multimedia">Multimedia</option>
                                        <option value="Electronic Resource">Electronic Resource</option>
                                        <option value="Digital Versatile Disc">Digital Versatile Disc</option>
                                    </select>
                                    <label>GMD</label>
                                </div>
                                <div class="input-field col s2">
                                    <select name="bahasa" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="English">English</option>
                                    </select>
                                    <label>Bahasa</label>
                                </div>
                                <div class="input-field col s3">
                                    <select name="jurusan" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="PPM">PPM</option>
                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                    </select>
                                    <label>Jurusan</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="penerbit" type="text" required class="validate" name="penerbit">
                                    <label for="penerbit">Penerbit</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="tahun" type="number" required class="validate" name="tahun">
                                    <label for="tahun">Tahun Terbit</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="tempat" type="text" class="validate" name="tempat">
                                    <label for="tempat">Tempat Terbit</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" class="materialize-textarea validate" 
                                    style="height: 21.2px;" name="subyek"></textarea>
                                    <label for="subyek">Subyek</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" class="materialize-textarea validate" 
                                    style="height: 21.2px;" name="deskripsi"></textarea>
                                    <label for="subyek">Deskripsi Fisik</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" class="materialize-textarea validate" 
                                    style="height: 21.2px;" name="letak"></textarea>
                                    <label for="subyek">Letak Buku</label>
                                </div>
                                <div class="file-field input-field col s12">
                                    <div class="btn teal lighten-1">
                                        <span>Pilih Thumbnail</span>
                                        <input type="file" name="thumbnail" accept=".png, .jpg, .jpeg">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Unggah thumbnail dengan format (.png, .jpg, .jpeg)">
                                    </div>
                                </div>
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light green btn" type="submit" name="btn_submit" value="1">Simpan</button>
                                    <button class="waves-effect waves-light orange btn" type="submit" name="btn_submit" value="2">Simpan Tambah Baru</button>
                                    <a href="operator/data-buku" class="waves-effect waves-light pink btn" type="button">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
</body>
</html>