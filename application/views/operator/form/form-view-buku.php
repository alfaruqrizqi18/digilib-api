<main class="mn-inner">
    <div class="row">
        <?php foreach ($data_buku as $data) { ?>
        <div class="col s12 m4 l3">
            <div class="card">
                <div class="card-content center-align">
                    <img src="<?php echo $data['thumbnail'] ?>" class="responsive-img" alt="">
                </div>
            </div>
        </div>
        <div class="col s12 m4 l9">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                    <span class="card-title">View Buku</span><br>
                        <form class="col s12" method="post">
                            <?php foreach ($data_buku as $data) { ?>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="judul" type="text" readonly class="" value="<?php echo $data['judul_buku'] ?>">
                                    <label for="judul">Judul Buku</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="pengarang" type="text" readonly class="" value="<?php echo $data['pengarang'] ?>">
                                    <label for="pengarang">Pengarang</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="edisi" type="text" readonly class="" value="<?php echo $data['edisi'] ?>">
                                    <label for="edisi">Edisi</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="no_panggil" type="text" readonly class="" value="<?php echo $data['no_panggil'] ?>">
                                    <label for="no_panggil">No. Panggil</label>
                                </div>
                                <div class="input-field col s4">
                                    <input id="isbn" type="text" readonly class="" value="<?php echo $data['isbn'] ?>">
                                    <label for="isbn">ISBN</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="gmd" type="text" readonly class="" value="<?php echo $data['gmd'] ?>">
                                    <label for="gmd">GMD</label>
                                </div>
                                <div class="input-field col s2">
                                    <input id="bahasa" type="text" readonly class="" value="<?php echo $data['bahasa'] ?>">
                                    <label for="bahasa">Bahasa</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="jurusan" type="text" readonly class="" value="<?php echo $data['jurusan'] ?>">
                                    <label for="jurusan">Jurusan</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="penerbit" type="text" readonly class="" value="<?php echo $data['penerbit'] ?>">
                                    <label for="penerbit">Penerbit</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="tahun" type="number" readonly class="" value="<?php echo $data['tahun_terbit'] ?>">
                                    <label for="tahun">Tahun Terbit</label>
                                </div>
                                <div class="input-field col s3">
                                    <input id="tempat" type="text" readonly class="" value="<?php echo $data['tempat_terbit'] ?>">
                                    <label for="tempat">Tempat Terbit</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" class="materialize-textarea" readonly 
                                    style="height: 21.2px;"><?php echo $data['subyek'] ?></textarea>
                                    <label>Subyek</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" class="materialize-textarea" readonly 
                                    style="height: 21.2px;"><?php echo $data['deskripsi_fisik'] ?></textarea>
                                    <label>Deskripsi Fisik</label>
                                </div>
                                <div class="input-field col s12">
                                    <textarea id="icon_prefix2" class="materialize-textarea" readonly 
                                    style="height: 21.2px;"><?php echo $data['tempat_terbit'] ?></textarea>
                                    <label>Letak Buku</label>
                                </div>
                                <div class="input-field col s12">
                                    <a href="operator/edit/buku/<?php echo $data['id_buku'] ?>" class="waves-effect waves-light orange btn">Edit</a>
                                    <a onclick="window.close()" class="waves-effect waves-light pink btn" type="button">Tutup</a>
                                </div>
                            </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
</body>
</html>