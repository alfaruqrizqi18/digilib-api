<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l8">
            <div class="card">
                <div class="card-content">
                      <?php if ($this->session->flashdata('error') != null): ?>
                        <div class="card-panel green ">
                          <span class="white-text">
                            <?php echo $this->session->flashdata('error'); ?>
                          </span>
                        </div>
                      <?php endif ?>
                    <div class="row">
                    <span class="card-title">Pengaturan</span><br>
                        <form class="col s12" method="post">
                            <div class="row">

                                <div class="input-field col s2">
                                    <?php foreach ($show_current_setting as $data) {?>
                                        <input id="max_peminjaman" type="number" required class="validate" name="max_peminjaman" placeholder="Maksimal Peminjaman " value="<?php echo $data->config_total_pinjam_mahasiswa ?>">
                                    <?php } ?>
                                    <label for="max_peminjaman">Max Peminjaman</label>
                                </div>

                                <div class="input-field col s2">
                                    <?php foreach ($show_current_setting as $data) {?>
                                        <input id="denda" type="number" required class="validate" name="denda" placeholder="Denda Per Hari " value="<?php echo $data->nominal_denda ?>">
                                    <?php } ?>
                                    <label for="max_peminjaman">Denda Per Hari</label>
                                </div>

                                <div class="input-field col s4">
                                    <select name="waktu_kadaluwarsa" required>
                                        <?php foreach ($show_current_setting as $data) {?>
                                            <?php if ($data->waktu_kadaluwarsa == 30): ?>
                                            <option value="<?php echo $data->waktu_kadaluwarsa; ?>" selected>Waktu saat ini : <?php echo $data->waktu_kadaluwarsa; ?> menit</option>
                                            <?php elseif ($data->waktu_kadaluwarsa == 60): ?>
                                            <option value="<?php echo $data->waktu_kadaluwarsa; ?>" selected>Waktu saat ini 1 jam</option>
                                            <?php elseif ($data->waktu_kadaluwarsa == 120): ?>
                                            <option value="<?php echo $data->waktu_kadaluwarsa; ?>" selected>Waktu saat ini 2 jam</option>
                                            <?php elseif ($data->waktu_kadaluwarsa == 180): ?>
                                            <option value="<?php echo $data->waktu_kadaluwarsa; ?>" selected>Waktu saat ini 3 jam</option>
                                            <?php endif ?>
                                        <?php } ?>
                                        <option value="30">30 Menit</option>
                                        <option value="60">1 Jam</option>
                                        <option value="120">2 Jam</option>
                                        <option value="180">3 Jam</option>
                                    </select>
                                    <label>Setting waktu kadaluwarsa</label>
                                </div>

                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light green btn" type="submit" name="btn_submit" value="1">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
</body>
</html>