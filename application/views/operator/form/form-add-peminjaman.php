<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-content">
                      <?php if ($this->session->flashdata('error') != null): ?>
                        <div class="card-panel red ">
                          <span class="white-text">
                            <?php echo $this->session->flashdata('error'); ?>
                          </span>
                        </div>
                      <?php endif ?>
                    <div class="row">
                    <span class="card-title">Form Transaksi Peminjaman Buku Baru</span><br>
                        <form class="col s12" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="input-field col s6">
                                    <select name="id_stok" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        <?php foreach ($data_stok_buku as $data): ?>
                                            <option value="<?php echo $data['id_stok'] ?>"><?php echo $data['judul_buku']." - ".$data['unique_id_buku']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label>Judul Buku</label>
                                </div>
                                <div class="input-field col s6">
                                    <select name="nim" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        <?php foreach ($data_mahasiswa as $data): ?>
                                            <option value="<?php echo $data['nim'] ?>"><?php echo $data['nim']." - ".$data['nama']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label>Nama Mahasiswa</label>
                                </div>
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light green btn" type="submit" name="btn_submit" value="1">Simpan</button>
                                    <button class="waves-effect waves-light orange btn" type="submit" name="btn_submit" value="2">Simpan Tambah Baru</button>
                                    <a href="operator/data-peminjaman-buku" class="waves-effect waves-light pink btn" type="button">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
</body>
</html>