<style type="text/css">
    table.dataTable td {
      word-break: break-word;
    }
</style>
<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">
                    <img class="light-blue" alt="" height="75px;">
                    <span class="card-title">Laporan - Data Master Laporan</span>
                </div>
                <div class="card-content">
                    <a href="operator/add/laporan" class="waves-effect waves-light light-blue btn">
                        <i class="material-icons">note_add</i>
                    </a>
                    <br><br>
                    <table id="example" class="display responsive-table datatable-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Tahun</th>
                                <th>Jenis Laporan</th>
                                <th>Jurusan</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Tahun</th>
                                <th>Jenis Laporan</th>
                                <th>Jurusan</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php 
                            $nomor = 1;
                            foreach ($data_laporan as $data) { ?>
                            <tr>
                                <td><?php echo $nomor; ?></td>
                                <td><?php echo $data['judul_laporan']; ?></td>
                                <td><?php echo $data['tahun']; ?></td>
                                <td><?php echo $data['jenis_laporan']; ?></td>
                                <td><?php echo $data['jurusan']; ?></td>
                                <td>
                                    <a class='dropdown-button btn blue' href='#' data-activates='dropdown-<?php echo $nomor; ?>'>Actions</a>
                                    <!-- Dropdown Structure -->
                                    <ul id='dropdown-<?php echo $nomor; ?>' class='dropdown-content'>
                                        <li><a href="<?php echo base_url('operator/edit/laporan/'.$data['id_laporan']) ?>">Edit</a></li>
                                        <li class="divider"></li>
                                        <li><a href="<?php echo base_url('operator/delete/laporan/'.$data['id_laporan'].'/'.base64_encode($data['alamat_file_laporan'])) ?>">Hapus</a></li>
                                    </ul>
                                </td>
                            </tr>
                            <?php $nomor++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable({
        responsive: true,
        // columnDefs: [
        //     { width: 400, targets: 1 }
        // ],
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        }
    });
    $('.dataTables_length select').addClass('browser-default');
});
</script>
</body>
</html>