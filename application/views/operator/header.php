<!DOCTYPE html>
<html lang="en">
<base href="<?php echo base_url() ?>">
    <head>
        
        <!-- Title -->
        <title>Operator | Digilib Politeknik Kediri</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="assets/css/icon.css" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">

            
        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <div class="loader-bg"></div>
        <div class="loader">
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-spinner-teal lighten-1">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mn-content">
            <header class="mn-header navbar-fixed">
                <nav class="light-blue darken-1">
                    <div class="nav-wrapper row">
                        <section class="material-design-hamburger navigation-toggle">
                            <a href="#" data-activates="slide-out" class="button-collapse show-on-large material-design-hamburger__icon">
                                <span class="material-design-hamburger__layer"></span>
                            </a>
                        </section>
                        <div class="header-title col s3">      
                            <a href="operator"><span class="chapter-title">Digilib Apps</span></a>
                        </div>
                    </div>
                </nav>
            </header>
            <aside id="slide-out" class="side-nav white">
                <div class="side-nav-wrapper">
                    <div class="sidebar-profile">
                        <div class="sidebar-profile-image">
                            <img src="assets/images/profile-image.png" class="circle" alt="">
                        </div>
                        <div class="sidebar-profile-info">
                            <a href="javascript:void(0);" class="account-settings-link">
                                <p><?php echo "Nama : ".$this->session->userdata('digilib_nama'); ?></p>
                                <span><?php echo "Username : ".$this->session->userdata('digilib_username'); ?><i class="material-icons right">arrow_drop_down</i></span>
                            </a>
                        </div>
                    </div>
                    <div class="sidebar-account-settings">
                        <ul>
                            <!-- <li class="no-padding">
                                <a href="operator/my-profile/" class="waves-effect waves-grey"><i class="material-icons">tag_faces</i>Profil</a>
                            </li> -->
                            <li class="no-padding">
                                <a href="auth/logout" class="waves-effect waves-grey"><i class="material-icons">exit_to_app</i>Keluar</a>
                            </li>
                        </ul>
                    </div>
                <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
                    <li class="no-padding ">
                        <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">view_list</i>Data Master<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="operator/data-buku/">Buku</a></li>
                                <li><a href="operator/data-stok-buku/"><i class="material-icons">trending_flat</i> Stok Buku</a></li>
                                <li><a href="operator/data-laporan/">Laporan</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="no-padding ">
                        <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">view_list</i>Data Transaksi<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="operator/data-pemesanan-buku/">Pemesanan Buku</a></li>
                                <li><a href="operator/data-peminjaman-buku/">Peminjaman Buku</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="no-padding ">
                        <a href="operator/pengaturan/" class="collapsible-header waves-effect waves-grey" ><i class="material-icons">settings</i>Pengaturan</a>
                    </li>
                </ul>
                <div class="footer">
                    <p class="copyright">Politeknik Kediri ©</p>
                    <a href="#!">Digilib Apps</a>
                </div>
                </div>
            </aside>