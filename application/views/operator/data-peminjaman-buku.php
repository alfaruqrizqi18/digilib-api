<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">
                    <img class="light-blue" alt="" height="75px;">
                    <span class="card-title">Buku - Data Master Peminjaman Buku</span>
                </div>
                <div class="card-content">
                    <a href="operator/add/peminjaman_buku" class="waves-effect waves-light light-blue btn">
                        <i class="material-icons">note_add</i>
                    </a>
                    <?php if ($this->session->flashdata('error') != null): ?>
                        <div class="card-panel red ">
                            <span class="white-text">
                                <?php echo $this->session->flashdata('error'); ?>
                            </span>
                        </div>
                    <?php endif ?>
                    <?php if ($this->session->flashdata('success') != null): ?>
                        <div class="card-panel green ">
                            <span class="white-text">
                                <?php echo $this->session->flashdata('success'); ?>
                            </span>
                        </div>
                    <?php endif ?>
                    <table id="example" class="display responsive-table datatable-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Transaksi</th>
                                <th>Judul Buku</th>
                                <th>No. Buku</th>
                                <th>Mahasiswa</th>
                                <th>Tgl Pinjam</th>
                                <th>Tgl Kembali</th>
                                <th>Buku Dikembalikan</th>
                                <th>Total Denda</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Tgl Transaksi</th>
                                <th>Judul Buku</th>
                                <th>No. Buku</th>
                                <th>Mahasiswa</th>
                                <th>Pinjam</th>
                                <th>Kembali</th>
                                <th>Dikembalikan</th>
                                <th>Total Denda</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php 
                            $nomor = 1;
                            foreach ($data_peminjaman_buku as $data) { ?>
                            <tr>
                                <td><?php echo $nomor; ?></td>
                                <td><?php echo date_format(date_create($data['tgl_transaksi']), "d M Y"); ?></td>
                                <td><?php echo $data['judul_buku']; ?></td>
                                <td><?php echo $data['unique_id_buku']; ?></td>
                                <td><?php echo $data['nama']; ?></td>
                                <td><?php echo date_format(date_create($data['tgl_pinjam_buku']), "d M Y"); ?></td>
                                <td><?php echo date_format(date_create($data['tgl_kembali_buku']), "d M Y"); ?></td>
                                <td><?php if ($data['tgl_buku_dikembalikan'] == "0000-00-00") {
                                    echo $data['tgl_buku_dikembalikan'];
                                } else {
                                    echo date_format(date_create($data['tgl_buku_dikembalikan']), "d M Y");
                                } ?>
                                    
                                </td>
                                <td>
                                    <b>
                                        <?php echo $data['total_denda']; ?>
                                    </b>
                                </td>

                                
                                <?php if ($data['status_transaksi'] == 'peminjaman_sedang_berlangsung'): ?>
                                    <td>
                                        <div class="chip waves-effect waves-blue blue" style="color: white;">Sedang Berlangsung</div>
                                    </td>

                                    <td>
                                        <a class='dropdown-button btn blue' href='#' data-activates='dropdown-<?php echo $nomor; ?>'>Actions</a>
                                        <ul id='dropdown-<?php echo $nomor; ?>' class='dropdown-content'>
                                            <li><a href="<?php echo base_url('operator/pengembalian_buku/'.$data['id_transaksi']); ?>">Buku telah dikembalikan</a></li>
                                            <li class="divider"></li>
                                        </ul>
                                    </td>
                                <?php elseif ($data['status_transaksi'] == 'peminjaman_melewati_batas'): ?>
                                    <td>
                                        <div class="chip waves-effect waves-red red" style="color: white;">Lewat Batas Waktu</div>
                                    </td>
                                    <td>
                                        <a class='dropdown-button btn blue' href='#' data-activates='dropdown-<?php echo $nomor; ?>'>Actions</a>
                                        <ul id='dropdown-<?php echo $nomor; ?>' class='dropdown-content'>
                                            <li><a href="<?php echo base_url('operator/pengembalian_buku/'.$data['id_transaksi']); ?>">Buku telah dikembalikan</a></li>
                                            <li class="divider"></li>
                                        </ul>
                                    </td>
                                <?php elseif ($data['status_transaksi'] == 'peminjaman_telah_berakhir'): ?>
                                    <td> 
                                        <div class="chip waves-effect waves-green green" style="color: white;">Peminjaman Berakhir</div>
                                    </td>
                                    <td></td>
                                <?php endif ?>

                                
                            </tr>
                            <?php $nomor++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable({
            responsive: true,
            columnDefs: [
            { width: 150, targets: 2 }
            ],
            language: {
                searchPlaceholder: 'Search records',
                sSearch: '',
                sLengthMenu: 'Show _MENU_',
                sLength: 'dataTables_length',
                oPaginate: {
                    sFirst: '<i class="material-icons">chevron_left</i>',
                    sPrevious: '<i class="material-icons">chevron_left</i>',
                    sNext: '<i class="material-icons">chevron_right</i>',
                    sLast: '<i class="material-icons">chevron_right</i>' 
                }
            }
        });
        $('.dataTables_length select').addClass('browser-default');
    });
</script>
</body>
</html>