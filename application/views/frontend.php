
<!doctype html>
<html lang="en">
<base href="<?php echo base_url() ?>">
<!--

Page    : index / MobApp
Version : 1.0
Author  : Colorlib
URI     : https://colorlib.com

 -->

<head>
    <title>Digilib Apps - Landing Pages</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Mobland - Mobile App Landing Page Template">
    <meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive">

    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/frontend/css/bootstrap.min.css">
    <!-- Themify Icons -->
    <link rel="stylesheet" href="assets/frontend/css/themify-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="assets/frontend/css/owl.carousel.min.css">
    <!-- Main css -->
    <link href="assets/frontend/css/style.css" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">

    <!-- Nav Menu -->

    <div class="nav-menu fixed-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-dark navbar-expand-lg">
                        <a class="navbar-brand" href="frontend">Digilib Apps</a> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                        <div class="collapse navbar-collapse" id="navbar">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item"><a href="apk/digilib-v1.0.apk" class="btn btn-outline-light my-3 my-sm-0 ml-lg-3">Download</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <header class="bg-gradient" id="home">
        <div class="container mt-5">
            <h1>Digilib Apps</h1>
            <p class="tagline">Aplikasi Perpustakaan Digital Politeknik Kediri</p>
        </div>
        <div class="img-holder mt-3"><img src="assets/frontend/images/1.png" alt="phone" class="img-fluid"></div>
    </header>


    <div class="section bg-gradient">
        <div class="container">
            <div class="call-to-action">

                <div class="box-icon"><span class="ti-mobile gradient-fill ti-3x"></span></div>
                <h2>Unduh dimanapun</h2>
                <p class="tagline">Digilib Apps tersedia di semua platform mobile.</p>
                <div class="my-4">

                    <a href="apk/digilib-v1.0.apk" class="btn btn-light"><img src="assets/frontend/images/appleicon.png" alt="icon"> App Store</a>
                    <a href="apk/digilib-v1.0.apk" class="btn btn-light"><img src="assets/frontend/images/playicon.png" alt="icon"> Google play</a>
                </div>
                <p class="text-primary"><small><i></i></small></p>
            </div>
        </div>

    </div>

    <!-- jQuery and Bootstrap -->
    <script src="assets/frontend/js/jquery-3.2.1.min.js"></script>
    <script src="assets/frontend/js/bootstrap.bundle.min.js"></script>
    <!-- Plugins JS -->
    <script src="assets/frontend/js/owl.carousel.min.js"></script>
    <!-- Custom JS -->
    <script src="assets/frontend/js/script.js"></script>

</body>

</html>
