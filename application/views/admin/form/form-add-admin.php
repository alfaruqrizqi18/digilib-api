<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l6">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                    <span class="card-title">Form Tambah Admin - Operator</span><br>
                        <form class="col s12" method="post">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="nama" type="text" required class="validate" name="nama">
                                    <label for="nama">Nama</label>
                                </div>
                                <div class="input-field col s6">
                                    <select name="level" required>
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="Admin">Admin</option>
                                        <option value="Operator">Operator</option>
                                    </select>
                                    <label>Level User</label>
                                </div>
                                <div class="input-field col s12">
                                    <input id="username" type="text" class="validate" name="username" required>
                                    <label for="username">Username</label>
                                </div>
                                <div class="input-field col s12">
                                    <input id="password" type="password" name="password" required class="validate">
                                    <label for="password">Password</label>
                                </div>
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light green btn" type="submit" name="btn_submit" value="1">Simpan</button>
                                    <button class="waves-effect waves-light orange btn" type="submit" name="btn_submit" value="2">Simpan Tambah Baru</button>
                                    <a href="admin" class="waves-effect waves-light pink btn" type="submit">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script src="assets/js/pages/table-data.js"></script>

</body>
</html>