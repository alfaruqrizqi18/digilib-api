<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l6">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                    <span class="card-title">Form Edit Mahasiswa</span><br>
                        <form class="col s12" method="post">
                            <?php foreach ($data_mahasiswa as $data) {?>
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="nim" type="text" required class="validate" name="nim" 
                                    value="<?php echo $data['nim'] ?>">
                                    <label for="nim">NIM</label>
                                </div>
                                <div class="input-field col s6">
                                    <select name="jurusan" required>
                                        <option value="" disabled >Choose your option</option>
                                        <option value="<?php echo $data['jurusan'] ?>" selected>Saat ini : <?php echo $data['jurusan'] ?></option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="PPM">PPM</option>
                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                    </select>
                                    <label>Jurusan</label>
                                </div>
                                <div class="input-field col s12">
                                    <input id="password" type="password" name="password" required class="validate" 
                                    value="<?php echo $data['password'] ?>">
                                    <label for="password">Password</label>
                                </div>
                                <div class="input-field col s12">
                                    <input id="nama" type="text" class="validate" name="nama" required 
                                    value="<?php echo $data['nama'] ?>">
                                    <label for="nama">Nama</label>
                                </div>
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light green btn" type="submit" name="btn_submit" value="1">Simpan</button>
                                    <a href="admin/data-mahasiswa" class="waves-effect waves-light pink btn" type="submit">Kembali</a>
                                </div>
                            </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script src="assets/js/pages/table-data.js"></script>

</body>
</html>