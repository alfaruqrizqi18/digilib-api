<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l6">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                    <span class="card-title">Form Edit Admin - Operator</span><br>
                        <?php foreach ($data_admin as $data) { ?>
                        <form class="col s12" method="post">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input id="nama" type="text" required class="validate" name="nama" 
                                    value="<?php echo $data['nama']; ?>">
                                    <label for="nama">Nama</label>
                                </div>
                                <div class="input-field col s6">
                                    <select name="level" required>
                                        <option value="" disabled >Choose your option</option>
                                        <option value="<?php echo $data['level']; ?>" selected>Level saat ini : <?php echo $data['level']; ?></option>
                                        <option value="Admin">Admin</option>
                                        <option value="Operator">Operator</option>
                                    </select>
                                    <label>Level User</label>
                                </div>
                                <div class="input-field col s12">
                                    <input id="username" type="text" class="validate" name="username" required 
                                    value="<?php echo $data['username']; ?>">
                                    <label for="username">Username</label>
                                </div>
                                <div class="input-field col s12">
                                    <input id="password" type="password" name="password" required class="validate"
                                    value="<?php echo $data['password']; ?>">
                                    <label for="password">Password</label>
                                </div>
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light green btn" type="submit" name="btn_submit" value="1">Simpan</button>
                                    <a href="admin" class="waves-effect waves-light pink btn" type="submit">Kembali</a>
                                </div>
                            </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script src="assets/js/pages/table-data.js"></script>

</body>
</html>