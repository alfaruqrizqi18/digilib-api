<main class="mn-inner">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card">
                <div class="card-image">
                    <img class="light-blue" alt="" height="75px;">
                    <span class="card-title">Mahasiswa - Data Master Mahasiswa</span>
                </div>
                <div class="card-content">
                    <a href="admin/add/mahasiswa" class="waves-effect waves-light light-blue btn">
                        <i class="material-icons">note_add</i>
                    </a>
                    <br><br>
                    <table id="example" class="display responsive-table datatable-example">
                        <thead>
                            <tr>
                                <th>Nomor</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Jurusan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Nomor</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Jurusan</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php 
                            $nomor = 1;
                            foreach ($data_mahasiswa as $data) { ?>
                            <tr>
                                <td><?php echo $nomor; ?></td>
                                <td><?php echo $data['nim']; ?></td>
                                <td><?php echo $data['nama']; ?></td>
                                <td><?php echo $data['jurusan']; ?></td>
                                <td>
                                    <a class='dropdown-button btn blue' href='#' data-activates='dropdown-<?php echo $nomor; ?>'>Actions</a>
                                    <!-- Dropdown Structure -->
                                    <ul id='dropdown-<?php echo $nomor; ?>' class='dropdown-content'>
                                        <li>
                                            <a href="<?php echo base_url('admin/edit/mahasiswa/'.$data['id_mahasiswa']) ?>">
                                                <i class="large material-icons">mode_edit</i>Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('admin/delete/mahasiswa/'.$data['id_mahasiswa']) ?>">
                                                <i class="large material-icons">delete</i>Hapus
                                            </a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <?php $nomor++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/js/alpha.min.js"></script>
<script src="assets/js/pages/table-data.js"></script>

</body>
</html>