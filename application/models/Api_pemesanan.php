<?php 
header('Content-Type: application/json');
/**
 * 
 */
 class Api_pemesanan extends CI_Model {
 	
 	function __construct(){
 		parent::__construct();
 		date_default_timezone_set('Asia/Jakarta');
 		$this->load->model("util_pemesanan");
 		$this->load->model("api_log");
 	}


 	function show_pemesanan_by_nim($nim) {
		$this->util_pemesanan->check_validitas_status_pemesanan();
 		$query = $this->db->query(
 			"SELECT pemesanan_tables.id_pemesanan, pemesanan_tables.nomor_pemesanan_unik, pemesanan_tables.tgl_pemesanan,
 					pemesanan_tables.pemesanan_dimulai_pada_jam, pemesanan_tables.pemesanan_hangus_pada_jam, 
 					pemesanan_tables.status,
 					buku_tables.judul_buku, buku_tables.thumbnail
 			 
 			 FROM pemesanan_tables
 			 
 			 LEFT JOIN stok_buku_tables ON stok_buku_tables.id_stok = pemesanan_tables.id_stok
 			 LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku

 			 WHERE buku_tables.id_buku = stok_buku_tables.id_buku
 			 AND stok_buku_tables.id_stok = pemesanan_tables.id_stok
 			 AND pemesanan_tables.nim = $nim
 			 ORDER BY id_pemesanan DESC
 			 "
 		);

 		if ($query->num_rows() > 0) {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'rows' => $query->num_rows(),
						'query_status' => 'OK',
						'results' => $query->result_array()
					)
				)
			);

 		} else {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'status' => 'NULL'
					)
				)
			);
 		}

		return $return;
 	}

 	function show_pemesanan_by_nim_filter($nim, $status) {
		$this->util_pemesanan->check_validitas_status_pemesanan();
 		$query = $this->db->query(
 			"SELECT pemesanan_tables.id_pemesanan, pemesanan_tables.nomor_pemesanan_unik, pemesanan_tables.tgl_pemesanan,
 					pemesanan_tables.pemesanan_dimulai_pada_jam, pemesanan_tables.pemesanan_hangus_pada_jam, 
 					pemesanan_tables.status,
 					buku_tables.judul_buku, buku_tables.thumbnail
 			 
 			 FROM pemesanan_tables
 			 
 			 LEFT JOIN stok_buku_tables ON stok_buku_tables.id_stok = pemesanan_tables.id_stok
 			 LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku

 			 WHERE buku_tables.id_buku = stok_buku_tables.id_buku
 			 AND stok_buku_tables.id_stok = pemesanan_tables.id_stok
 			 AND pemesanan_tables.nim = $nim
 			 AND pemesanan_tables.status = '$status'
 			 ORDER BY id_pemesanan DESC
 			 "
 		);

 		if ($query->num_rows() > 0) {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'rows' => $query->num_rows(),
						'query_status' => 'OK',
						'results' => $query->result_array()
					)
				)
			);

 		} else {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'status' => 'NULL'
					)
				)
			);
 		}

		return $return;
 	}

 	function add_pemesanan() {
 		//LOGIC
 		//1. CHECK APAKAH SUDAH PERNAH PESAN BUKU TERSEBUT DI HARI YANG SAMA DAN MASIH BERSTATUS WAITING
 		//2. GET RANDOM ID STOK BUKU BERDASARKAN ID BUKU
 		//3. JIKA JUMLAH BARIS YANG DITEMUKAN = 1 MAKA LANJUT
 		//4. INSERT KE TABLE PEMESANAN
 		//5. UPDATE IS_AVAILABLE STOK BUKU MENJADI FALSE
 		$val_waktu_kadaluwarsa;
 		$query_get_waktu_kadaluwarsa = $this->db->get('config_tables')->result();
 		foreach ($query_get_waktu_kadaluwarsa as $data_waktu_kadaluwarsa) {
 			$val_waktu_kadaluwarsa = $data_waktu_kadaluwarsa->waktu_kadaluwarsa;
 		}
 		$date_now = date('Y-m-d');
 		$nim = $this->input->post('nim');
 		$id_buku = $this->input->post('id_buku');
 		$tgl_pemesanan = $date_now;
 		$dateTime = strtotime(date('H:i').'+'.$val_waktu_kadaluwarsa.' minute');
 		$jam_hangus = date('H:i', $dateTime);

 		$query_check_pemesanan_ganda = $this->db->query(
 			"SELECT id_buku
 			FROM pemesanan_tables
 			WHERE id_buku = '$id_buku'
 			AND nim = '$nim'
 			AND status = 'waiting'
 			LIMIT 0,1
 			"
 		);

 		if ($query_check_pemesanan_ganda->num_rows() == 0 ) {
 		 	$query_get_random_id_stok_by_id_buku = 
	 			$this->
	 			db->
	 			query(
	 				"SELECT *
	 				 FROM stok_buku_tables
	 				 WHERE id_buku = $id_buku
	 				 AND is_available = 'true'
	 				 ORDER BY RAND()
	 				 LIMIT 0,1
	 				"
	 			);

	 		if ($query_get_random_id_stok_by_id_buku->num_rows() > 0) {

	 			$result_id_stok;
	 			$unique_id_buku;

	 			foreach ($query_get_random_id_stok_by_id_buku->result() as $data) {
	 				$result_id_stok = $data->id_stok;
	 				$unique_id_buku = $data->unique_id_buku;
	 			}
	 			$uniqid = uniqid();
	 			$data = array(
		 			'nomor_pemesanan_unik' => $uniqid,
		 			'id_buku' => $id_buku,
		 			'id_stok' => $result_id_stok,
		 			'nim' => $nim,
		 			'tgl_pemesanan' => $tgl_pemesanan,
		 			'pemesanan_dimulai_pada_jam' => date('H:i'),
		 			'pemesanan_hangus_pada_jam' => $jam_hangus,
		 			'status' => 'waiting'
		 		);
		 		$this->db->insert('pemesanan_tables', $data);
		 		$last_id = $this->db->insert_id();
		 		$this->api_log->add_log_type_1($nim, $result_id_stok);

		 		$data_show = array(
		 			'nomor_pemesanan_unik' => $uniqid,
		 			'id_buku' => $id_buku,
		 			'id_stok' => $result_id_stok,
		 			'nim' => $nim,
		 			'tgl_pemesanan' => $tgl_pemesanan,
		 			'pemesanan_dimulai_pada_jam' => date('H:i'),
		 			'pemesanan_hangus_pada_jam' => $jam_hangus,
		 			'status' => 'waiting',
		 			'id_pemesanan' => $last_id
		 		);
		 		$this->update_is_available_stok_buku($result_id_stok);

	 			$return = 
	 				$this->
	 				output->
	 				set_output(
						json_encode(
							array(
								'rows' => $query_get_random_id_stok_by_id_buku->num_rows(),
								'status' => 'OK',
								'result_id_stok' => $result_id_stok,
								'results' => [ $data_show ]
								// 'results' => $val_waktu_kadaluwarsa
							)
						)
					);

	 		} else {

	 			$return = 
	 				$this->
	 				output->
	 				set_output(
						json_encode(
							array(
								'status' => 'NULL',
								'results' => []
							)
						)
					);

	 		}

 		} else {

 			$return = 
 				$this->
 				output->
 				set_output(
					json_encode(
						array(
							'status' => 'DUPLICATE',
							'results' => []
						)
					)
				);
 		}
	 	return $return;	 		
 	}


 	function update_is_available_stok_buku($result_id_stok){
 		$data_update_is_available_for_stok_buku = array(
 			'is_available' => 'false' 
 		);
 		$this->db->where('id_stok', $result_id_stok);
 		$this->db->update('stok_buku_tables', $data_update_is_available_for_stok_buku);
 	}

 	function detail_pemesanan($id) {
 		$jam_skrg = date('H:i');
 		$query = $this->db->query(
 			"SELECT pemesanan_tables.id_pemesanan, pemesanan_tables.nomor_pemesanan_unik, pemesanan_tables.tgl_pemesanan,
 					pemesanan_tables.pemesanan_dimulai_pada_jam, pemesanan_tables.pemesanan_hangus_pada_jam, 
 					pemesanan_tables.status,
 					buku_tables.judul_buku, buku_tables.thumbnail

 			 
 			 FROM pemesanan_tables
 			 
 			 LEFT JOIN stok_buku_tables ON stok_buku_tables.id_stok = pemesanan_tables.id_stok
 			 LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku

 			 WHERE buku_tables.id_buku = stok_buku_tables.id_buku
 			 AND stok_buku_tables.id_stok = pemesanan_tables.id_stok
 			 AND pemesanan_tables.id_pemesanan = $id
 			 "
 		);

 		if ($query->num_rows() == 1) {
 			foreach ($query->result() as $data) {
 				$jam_hangus = $data->pemesanan_hangus_pada_jam;
 				if ($data->status == 'waiting') {
 					$time_remaining = $jam_hangus - $jam_skrg . " jam lagi";
 				} else if ($data->status == 'success'){
 					$time_remaining = "Telah berakhir";
 				} else {
 					$time_remaining = "Kadaluwarsa";
 				}
 			}
	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'rows' => $query->num_rows(),
						'query_status' => 'OK',
						'time_remaining' => $time_remaining,
						'results' => $query->result_array()
					)
				)
			);

 		} else {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'status' => 'NULL'
					)
				)
			);
 		}

		return $return;
 	}

 	function cancel_pemesanan(){
 		$id_pemesanan = $this->input->post('id_pemesanan');
 		$query = $this->db->query(
 			"SELECT id_stok
 			 FROM pemesanan_tables
 			 WHERE id_pemesanan = $id_pemesanan
 			 LIMIT 0,1"
 		);

 		foreach ($query->result() as $data) {
 			$id_stok = $data->id_stok;
 			$data_update_is_available_for_stok_buku = array(
 				'is_available' => "true"
 			);
 			$this->db->where('id_stok', $id_stok);
 			$this->db->update('stok_buku_tables', $data_update_is_available_for_stok_buku);
 			$this->db->delete('pemesanan_tables', array('id_pemesanan' => $id_pemesanan));
 		}
 	}

 } 

 ?>