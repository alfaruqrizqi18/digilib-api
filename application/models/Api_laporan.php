<?php /**
* 
*/
class Api_laporan extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	function get_all_laporan(){
		$this->db->order_by('id_laporan', 'desc');
		$query = $this->db->get('laporan_tables');
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function get_laporan_for_landingpage_laporan(){
		$query = $this->db->query(
			"SELECT * 
			FROM laporan_tables
			ORDER BY RAND()
			LIMIT 0,10"
		);
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function get_laporan_by_param($param){
		$query = $this->db->query(
			"SELECT * 
			FROM laporan_tables
			WHERE 
			judul_laporan LIKE '%$param'
			OR judul_laporan LIKE '%$param%'
			OR judul_laporan LIKE '$param%'

			OR jenis_laporan LIKE '%$param'
			OR jenis_laporan LIKE '%$param%'
			OR jenis_laporan LIKE '$param%'

			OR jurusan LIKE '%$param'
			OR jurusan LIKE '%$param%'
			OR jurusan LIKE '$param%'

			OR tahun LIKE '%$param'
			OR tahun LIKE '%$param%'
			OR tahun LIKE '$param%'
			");
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}
} 
?>