<?php 

/**
* 
*/
class Util_notifikasi extends CI_Model{
	
	function __construct() {
		parent::__construct();
 		date_default_timezone_set('Asia/Jakarta');
	}

	function kirim_notifikasi($token_android, $judul_pesan, $pesan, $last_id_notifikasi, $tgl_notifikasi){
		$url = 'https://fcm.googleapis.com/fcm/send';

		$headers = array(
			'Authorization:key = AIzaSyBZ5P5M4BgNz4fHsYfgg5yE6QoWj6DuuX4',
			'Content-Type: application/json'
		);

		$notification = array(
			'body' => "Lihat! Ada pemberitahuan terbaru dari Digilib Assistant...",
			'title' => 'Digilib Assistant',
			'click_action' => 'digilibapp.morfin.com.digilibapp_TARGET_NOTIFICATION',
			'sound' => "default"
		);
		$data = array(
			'id_notifikasi' => $last_id_notifikasi,
			'judul_notifikasi' => $judul_pesan,
			'pesan_notifikasi' => $pesan,
			'tgl_notifikasi' => $tgl_notifikasi
		);
		$fields = array(
			'to' => $token_android,
			'priority' => 'high',
			'notification' => $notification,
			'data' => $data
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);           
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		curl_close($ch);
		// echo $result;
	}

	function notifikasi_type_1(){
		$date_now = date('Y-m-d');
		$time_now = date('H:i');
		$query = $this->db->query(
			"SELECT mahasiswa_users.token_android, mahasiswa_users.nim,
					transaksi_tables.tgl_kembali_buku,
					buku_tables.judul_buku
			 FROM transaksi_tables
			 LEFT JOIN stok_buku_tables ON transaksi_tables.id_stok = stok_buku_tables.id_stok
			 LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
			 LEFT JOIN mahasiswa_users ON transaksi_tables.nim = mahasiswa_users.nim
			 WHERE tgl_kembali_buku = '$date_now' + INTERVAL 1 DAY
			 AND status_transaksi = 'peminjaman_sedang_berlangsung'
			"
		);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$nim = $data->nim;
				$token_android = $data->token_android;
				$judul_buku = $data->judul_buku;
				$judul_pesan = $judul_buku;
				$pesan = "Jangan lupa besok waktunya mengembalikan buku dengan judul ".$judul_buku;
				$data_insert_notifikasi = array(
					'nim' => $nim,
					'judul_notifikasi' => $judul_pesan,
					'pesan_notifikasi' => $pesan,
					'token_android' => $token_android,
					'tgl_notifikasi' => $date_now 
				);
				$this->db->insert('notifikasi_tables', $data_insert_notifikasi);
				$last_id_notifikasi = $this->db->insert_id();
				//====
				$this->kirim_notifikasi($token_android, $judul_pesan, $pesan, $last_id_notifikasi, $date_now);
			}
		}
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function notifikasi_type_2(){
		$date_now = date('Y-m-d');
		$query = $this->db->query(
			"SELECT mahasiswa_users.token_android, mahasiswa_users.nim,
					transaksi_tables.tgl_kembali_buku,
					buku_tables.judul_buku
			 FROM transaksi_tables
			 LEFT JOIN stok_buku_tables ON transaksi_tables.id_stok = stok_buku_tables.id_stok
			 LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
			 LEFT JOIN mahasiswa_users ON transaksi_tables.nim = mahasiswa_users.nim
			 WHERE tgl_kembali_buku = '$date_now'
			 AND status_transaksi = 'peminjaman_sedang_berlangsung'
			"
		);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$nim = $data->nim;
				$token_android = $data->token_android;
				$judul_buku = $data->judul_buku;
				$judul_pesan = $judul_buku;
				$pesan = "Hari ini adalah hari terakhir kamu untuk mengembalikan buku yang berjudul ".$judul_buku.". Jangan lupa ya...";
				$data_insert_notifikasi = array(
					'nim' => $nim,
					'judul_notifikasi' => $judul_pesan,
					'pesan_notifikasi' => $pesan,
					'token_android' => $token_android,
					'tgl_notifikasi' => $date_now 
				);
				$this->db->insert('notifikasi_tables', $data_insert_notifikasi);
				$last_id_notifikasi = $this->db->insert_id();
				//====
				$this->kirim_notifikasi($token_android, $judul_pesan, $pesan, $last_id_notifikasi, $date_now);
			}
		}
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function notifikasi_type_3(){
		$date_now = date('Y-m-d');
		$query = $this->db->query(
			"SELECT mahasiswa_users.token_android, mahasiswa_users.nim,
					transaksi_tables.tgl_kembali_buku,
					buku_tables.judul_buku
			 FROM transaksi_tables
			 LEFT JOIN stok_buku_tables ON transaksi_tables.id_stok = stok_buku_tables.id_stok
			 LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
			 LEFT JOIN mahasiswa_users ON transaksi_tables.nim = mahasiswa_users.nim
			 WHERE tgl_kembali_buku < '$date_now'
			 AND status_transaksi = 'peminjaman_melewati_batas'
			 OR status_transaksi = 'peminjaman_sedang_berlangsung'
			"
		);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$nim = $data->nim;
				$token_android = $data->token_android;
				$judul_buku = $data->judul_buku;
				$judul_pesan = $judul_buku;
				$pesan = "Transaksi peminjaman buku yang berjudul ".$judul_buku." sudah melewati batas. Ayo segera kembalikan agar dendanya tidak terlalu banyak...";
				$data_insert_notifikasi = array(
					'nim' => $nim,
					'judul_notifikasi' => $judul_pesan,
					'pesan_notifikasi' => $pesan,
					'token_android' => $token_android,
					'tgl_notifikasi' => $date_now 
				);
				$this->db->insert('notifikasi_tables', $data_insert_notifikasi);
				$last_id_notifikasi = $this->db->insert_id();
				//====
				$this->kirim_notifikasi($token_android, $judul_pesan, $pesan, $last_id_notifikasi, $date_now);
			}
		}
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}


}
 ?>