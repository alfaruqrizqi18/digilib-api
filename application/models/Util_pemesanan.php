<?php 
/**
* 
*/
class Util_pemesanan extends CI_Model{
	
	function __construct(){
		parent::__construct();
 		date_default_timezone_set('Asia/Jakarta');
	}

	function check_validitas_status_pemesanan(){
		$date_now = date('Y-m-d');
		$time_now = date('H:i');

		$query = $this->db->query(
			"SELECT *
			 FROM pemesanan_tables
			 WHERE tgl_pemesanan = '$date_now'
			 AND pemesanan_hangus_pada_jam <  '$time_now'
			 AND status = 'waiting'
			"
		);
		
		foreach ($query->result() as $data) {
			$id_pemesanan = $data->id_pemesanan;
			$id_stok = $data->id_stok;
			$status_pemesanan = $data->status;


			$data_update_stok_buku_is_available = array(
				'is_available' => 'true'  
			);

			$data_update_status_pemesanan = array(
				'status' => 'expired'
			);

			$this->db->where('id_stok', $id_stok);
			$this->db->update('stok_buku_tables', $data_update_stok_buku_is_available);

			$this->db->where('id_pemesanan', $id_pemesanan);
			$this->db->update('pemesanan_tables', $data_update_status_pemesanan);
		}
	}
}
 ?>