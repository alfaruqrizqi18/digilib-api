<?php /**
* 
*/
class Api_peminjaman extends CI_Model {
	
	function __construct() {
		parent::__construct();
		$this->load->model("util_peminjaman");
	}

	function show_all_peminjaman_by_nim($nim){
		$this->util_peminjaman->check_status_transaksi();
		$query = $this->db->query(
			"SELECT transaksi_tables.id_transaksi, transaksi_tables.tgl_transaksi, transaksi_tables.tgl_pinjam_buku,
					transaksi_tables.tgl_kembali_buku, transaksi_tables.tgl_buku_dikembalikan, transaksi_tables.status_transaksi,
					transaksi_tables.total_denda,

					mahasiswa_users.nama,

					buku_tables.judul_buku, buku_tables.thumbnail
			FROM transaksi_tables

			LEFT JOIN mahasiswa_users ON mahasiswa_users.nim = transaksi_tables.nim
			LEFT JOIN stok_buku_tables ON transaksi_tables.id_stok = stok_buku_tables.id_stok
			LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
			WHERE transaksi_tables.nim = $nim
			ORDER BY id_transaksi DESC"
		);
		if ($query->num_rows() > 0) {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'rows' => $query->num_rows(),
						'query_status' => 'OK',
						'results' => $query->result_array()
					)
				)
			);

 		} else {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'status' => 'NULL'
					)
				)
			);
 		}

		return $return;
	}

	function show_peminjaman_by_nim_filter($nim, $status_transaksi){
		$this->util_peminjaman->check_status_transaksi();
		$query = $this->db->query(
			"SELECT transaksi_tables.id_transaksi, transaksi_tables.tgl_transaksi, transaksi_tables.tgl_pinjam_buku,
					transaksi_tables.tgl_kembali_buku, transaksi_tables.tgl_buku_dikembalikan, transaksi_tables.status_transaksi,
					transaksi_tables.total_denda,

					mahasiswa_users.nama,

					buku_tables.judul_buku, buku_tables.thumbnail
			FROM transaksi_tables

			LEFT JOIN mahasiswa_users ON mahasiswa_users.nim = transaksi_tables.nim
			LEFT JOIN stok_buku_tables ON transaksi_tables.id_stok = stok_buku_tables.id_stok
			LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
			WHERE transaksi_tables.nim = $nim
			AND status_transaksi = '$status_transaksi'
			ORDER BY id_transaksi DESC"
		);
		if ($query->num_rows() > 0) {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'rows' => $query->num_rows(),
						'query_status' => 'OK',
						'results' => $query->result_array()
					)
				)
			);

 		} else {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'status' => 'NULL'
					)
				)
			);
 		}

		return $return;
	}

	function detail_peminjaman($id_transaksi) {
		$query = $this->db->query(
			"SELECT transaksi_tables.id_transaksi, transaksi_tables.tgl_transaksi, transaksi_tables.tgl_pinjam_buku,
					transaksi_tables.tgl_kembali_buku, transaksi_tables.tgl_buku_dikembalikan, transaksi_tables.status_transaksi,
					transaksi_tables.total_denda,

					mahasiswa_users.nama,

					buku_tables.judul_buku, buku_tables.thumbnail
			FROM transaksi_tables

			LEFT JOIN mahasiswa_users ON mahasiswa_users.nim = transaksi_tables.nim
			LEFT JOIN stok_buku_tables ON transaksi_tables.id_stok = stok_buku_tables.id_stok
			LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
			WHERE transaksi_tables.id_transaksi = $id_transaksi
			ORDER BY id_transaksi DESC
			LIMIT 0,1"
		);
		if ($query->num_rows() > 0) {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'rows' => $query->num_rows(),
						'query_status' => 'OK',
						'results' => $query->result_array()
					)
				)
			);

 		} else {

	 		$return = 
				$this->
				output->
				set_output(
				json_encode(
					array(
						'status' => 'NULL'
					)
				)
			);
 		}

		return $return;
	}

} 

?>