<?php 
/**
 * 
 */
class Admin_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	function add($param){
		if ($param == 'adminoperator') {

			$nama = $this->input->post('nama', TRUE);
			$level = $this->input->post('level', TRUE);
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);

			$data = array(
				'nama' => $nama,
				'level' => $level,
				'username' => $username,
				'password' => $password, 
			);
			$this->db->insert('admin_users', $data);

			if ($this->input->post('btn_submit') == 1) {
				redirect(base_url('admin'));
			} else if ($this->input->post('btn_submit') == 2) {
				redirect(base_url('admin/add/adminoperator'));
			}

		} else if ($param == 'mahasiswa') {
			$val_max_pinjam;
	 		$query_get_max_pinjam= $this->db->get('config_tables')->result();
	 		foreach ($query_get_max_pinjam as $data_max_pinjam) {
	 			$val_max_pinjam = $data_max_pinjam->config_total_pinjam_mahasiswa;
	 		}
			$nama = $this->input->post('nama', TRUE);
			$jurusan = $this->input->post('jurusan', TRUE);
			$nim = $this->input->post('nim', TRUE);
			$password = $this->input->post('password', TRUE);

			$data = array(
				'nama' => $nama,
				'jurusan' => $jurusan,
				'nim' => $nim,
				'password' => $password,
				'token_android' => 'first_token',
				'kesempatan_pinjam_buku' => $val_max_pinjam
			);
			$this->db->insert('mahasiswa_users', $data);

			if ($this->input->post('btn_submit') == 1) {
				redirect(base_url('admin/data-mahasiswa'));
			} else if ($this->input->post('btn_submit') == 2) {
				redirect(base_url('admin/add/mahasiswa'));
			}

		}
	}

	function show($param){
		if ($param == 'adminoperator') {
			$query = $this->db->get('admin_users')->result_array();
		} else if ($param == 'mahasiswa') {
			$query = $this->db->get('mahasiswa_users')->result_array();
		}
		return $query;
	}

	function show_by_id($param, $id) {
		if ($param == 'adminoperator') {
			$query = $this->db->get_where('admin_users', array('id_admin' => $id))->result_array();
		} else if ($param == 'mahasiswa') {
			$query = $this->db->get_where('mahasiswa_users', array('id_mahasiswa' => $id))->result_array();
		}
		return $query;
	}

	function edit($param, $id) {
		if ($param == 'adminoperator') {
			$nama = $this->input->post('nama', TRUE);
			$level = $this->input->post('level', TRUE);
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);

			$data = array(
				'nama' => $nama,
				'level' => $level,
				'username' => $username,
				'password' => $password, 
			);
			$this->db->where('id_admin', $id);
			$this->db->update('admin_users', $data);
			redirect(base_url('admin'));
		} else if ($param == 'mahasiswa') {
			$nama = $this->input->post('nama', TRUE);
			$jurusan = $this->input->post('jurusan', TRUE);
			$nim = $this->input->post('nim', TRUE);
			$password = $this->input->post('password', TRUE);

			$data = array(
				'nama' => $nama,
				'jurusan' => $jurusan,
				'nim' => $nim,
				'password' => $password
			);

			$this->db->where('id_mahasiswa', $id);
			$this->db->update('mahasiswa_users', $data);
			redirect(base_url('admin/data-mahasiswa'));
		}
	}

	function delete($param, $id){
		if ($param == 'adminoperator') {
			$data = array(
				'id_admin' => $id 
			);
			$query = $this->db->delete('admin_users', $data);
			redirect(base_url('admin'));
		} else if ($param == 'mahasiswa') {
			$data = array(
				'id_mahasiswa' => $id 
			);
			$query = $this->db->delete('mahasiswa_users', $data);
			redirect(base_url('admin/data-mahasiswa'));
		}
	}
} 
?>