<?php 
/**
* 
*/
class Operator_model extends CI_Model {
	
	function __construct(){
 		parent::__construct();
 		date_default_timezone_set('Asia/Jakarta');
 		$this->load->model("util_pemesanan");
		$this->load->model("util_peminjaman");
 		$this->load->model("api_log");

		//utilitas//
		$this->util_pemesanan->check_validitas_status_pemesanan();
		$this->util_peminjaman->check_status_transaksi();
		//utilitas//
	}

	function show($param) {
		if ($param == 'buku') {
			$this->db->order_by('id_buku', 'desc');
			$query = $this->db->get('buku_tables')->result_array();
		} else if ($param == 'laporan') {
			$this->db->order_by('id_laporan', 'desc');
			$query = $this->db->get('laporan_tables')->result_array();
		} else if ($param == 'pemesanan_buku') {
			$query = $this->show_pemesanan()->result_array();
		} else if ($param == 'peminjaman_buku') {
			//utilitas//
			//utilitas//
			$query = $this->show_all_peminjaman()->result_array();
		}
		return $query;
	}

	function show_by_id($param, $id){
		if ($param == 'buku') {
			$query = $this->db->get_where('buku_tables', array('id_buku' => $id))->result_array();
		} else if ($param == 'laporan') {
			$query = $this->db->get_where('laporan_tables', array('id_laporan' => $id))->result_array();
		} elseif ($param == 'stok_buku') {
			$query = $this->db->query(
				"SELECT buku_tables.judul_buku,
						stok_buku_tables.id_stok, stok_buku_tables.id_buku, stok_buku_tables.unique_id_buku, 
						stok_buku_tables.is_available
				 FROM stok_buku_tables

				 LEFT JOIN buku_tables ON stok_buku_tables.id_buku = buku_tables.id_buku

				 WHERE stok_buku_tables.id_buku = '$id'"
			)->result_array();
		}
		return $query;
	}

	function add($param) {
		if ($param == 'buku') {
			$this->add_buku();
		} else if ($param == 'laporan') {
			$this->add_laporan();
		} else if ($param == 'peminjaman_buku') {
			$this->add_peminjaman();
		} elseif ($param == 'stok_buku') {
			$this->add_stok();
		}
	}

	function edit($param, $id) {
		if ($param == 'buku') {
			$this->edit_buku($id);
		} else if ($param == 'laporan') {
			$this->edit_laporan($id);
		} else if ($param == 'pemesanan_buku') {
			
		} else if ($param == 'peminjaman_buku') {
			
		}
	}

	function delete($param, $id, $url_file_path) {
		if ($param == 'buku') {
			$data = array(
				'id_buku' => $id);
			$this->db->delete('buku_tables', $data);
			unlink(base64_decode($url_file_path));
			redirect(base_url('operator/data-buku'));
		} else if ($param == 'laporan') {
			$data = array(
				'id_laporan' => $id);
			$this->db->delete('laporan_tables', $data);
			unlink(base64_decode($url_file_path));
			redirect(base_url('operator/data-laporan'));
		} else if ($param == 'pemesanan_buku') {
			
		} else if ($param == 'peminjaman_buku') {
			
		}
	}
	//CRUD BUKU
	function add_buku(){
		$judul_buku = $this->input->post('judul', TRUE);
		$pengarang = $this->input->post('pengarang', TRUE);
		$edisi = $this->input->post('edisi', TRUE);
		$jurusan = $this->input->post('jurusan', TRUE);
		$no_panggil = $this->input->post('no_panggil', TRUE);
		$isbn = $this->input->post('isbn', TRUE);
		$gmd = $this->input->post('gmd', TRUE);
		$bahasa = $this->input->post('bahasa', TRUE);
		$penerbit = $this->input->post('penerbit', TRUE);
		$tahun_terbit = $this->input->post('tahun', TRUE);
		$tempat_terbit = $this->input->post('tempat', TRUE);
		$subyek = $this->input->post('subyek', TRUE);
		$deskripsi_fisik = $this->input->post('deskripsi', TRUE);
		$letak_buku = $this->input->post('letak', TRUE);

		$config = array(
			'upload_path' => "./thumbnail-buku/",
			'allowed_types' => 'png|jpg|jpeg',
			'encrypt_name' => TRUE
		);
		$this->load->library('upload', $config);
		if ($_FILES['thumbnail']['name'] == "") {
    		$data = array(
            	'judul_buku' => $judul_buku,
            	'pengarang' => $pengarang,
            	'edisi' => $edisi,
            	'jurusan' => $jurusan,
            	'no_panggil' => $no_panggil,
            	'isbn' => $isbn,
            	'gmd' => $gmd,
            	'bahasa' => $bahasa,
            	'penerbit' => $penerbit,
            	'tahun_terbit' => $tahun_terbit,
            	'tempat_terbit' => $tempat_terbit,
            	'subyek' => $subyek,
            	'deskripsi_fisik' => $deskripsi_fisik,
            	'letak_buku' => $letak_buku,
            	'thumbnail' => 'thumbnail-buku/default.png'
            );
            $this->db->insert('buku_tables', $data);
            if ($this->input->post('btn_submit') == 1) {
				redirect(base_url('operator/data-buku'));
			} else if ($this->input->post('btn_submit') == 2) {
				redirect(base_url('operator/add/buku'));
			}
    	} else {
			if (!$this->upload->do_upload('thumbnail')) {
	            $error = $this->upload->display_errors();
	            $this->session->set_flashdata('error', $error);
	            redirect(base_url('operator/add/buku'));
	        } else {
	    		$result = $this->upload->data();
	            $data = array(
	            	'judul_buku' => $judul_buku,
	            	'pengarang' => $pengarang,
	            	'edisi' => $edisi,
	            	'jurusan' => $jurusan,
	            	'no_panggil' => $no_panggil,
	            	'isbn' => $isbn,
	            	'gmd' => $gmd,
	            	'bahasa' => $bahasa,
	            	'penerbit' => $penerbit,
	            	'tahun_terbit' => $tahun_terbit,
	            	'tempat_terbit' => $tempat_terbit,
	            	'subyek' => $subyek,
	            	'deskripsi_fisik' => $deskripsi_fisik,
	            	'letak_buku' => $letak_buku,
	            	'thumbnail' => 'thumbnail-buku/'.$result['file_name'],
	            	'alamat_thumbnail' => $result['full_path']
	            );
	            $this->db->insert('buku_tables', $data);
	            if ($this->input->post('btn_submit') == 1) {
					redirect(base_url('operator/data-buku'));
				} else if ($this->input->post('btn_submit') == 2) {
					redirect(base_url('operator/add/buku'));
				}
	        }
    	}
	}

	function edit_buku($id){
		$judul_buku = $this->input->post('judul', TRUE);
		$pengarang = $this->input->post('pengarang', TRUE);
		$edisi = $this->input->post('edisi', TRUE);
		$jurusan = $this->input->post('jurusan', TRUE);
		$no_panggil = $this->input->post('no_panggil', TRUE);
		$isbn = $this->input->post('isbn', TRUE);
		$gmd = $this->input->post('gmd', TRUE);
		$bahasa = $this->input->post('bahasa', TRUE);
		$penerbit = $this->input->post('penerbit', TRUE);
		$tahun_terbit = $this->input->post('tahun', TRUE);
		$tempat_terbit = $this->input->post('tempat', TRUE);
		$subyek = $this->input->post('subyek', TRUE);
		$deskripsi_fisik = $this->input->post('deskripsi', TRUE);
		$letak_buku = $this->input->post('letak', TRUE);

		$thumbnail_baru = $_FILES['thumbnail_baru']['name'];
		if ($thumbnail_baru == "") {
			$data = array(
            	'judul_buku' => $judul_buku,
            	'pengarang' => $pengarang,
            	'edisi' => $edisi,
            	'jurusan' => $jurusan,
            	'no_panggil' => $no_panggil,
            	'isbn' => $isbn,
            	'gmd' => $gmd,
            	'bahasa' => $bahasa,
            	'penerbit' => $penerbit,
            	'tahun_terbit' => $tahun_terbit,
            	'tempat_terbit' => $tempat_terbit,
            	'subyek' => $subyek,
            	'deskripsi_fisik' => $deskripsi_fisik,
            	'letak_buku' => $letak_buku
            );
            $this->db->where('id_buku', $id);
            $this->db->update('buku_tables', $data);
            redirect(base_url('operator/data-buku'));
		} else {
			$config = array(
				'upload_path' => "./thumbnail-buku/",
				'allowed_types' => 'png|jpg|jpeg',
				'encrypt_name' => TRUE
			);
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('thumbnail_baru')) {
	            $error = $this->upload->display_errors();
	            $this->session->set_flashdata('error', $error);
	            redirect(base_url('operator/edit/buku/'.$id));
	        } else {
	            $result = $this->upload->data();
	            $data = array(
	            	'judul_buku' => $judul_buku,
	            	'pengarang' => $pengarang,
	            	'edisi' => $edisi,
	            	'jurusan' => $jurusan,
	            	'no_panggil' => $no_panggil,
	            	'isbn' => $isbn,
	            	'gmd' => $gmd,
	            	'bahasa' => $bahasa,
	            	'penerbit' => $penerbit,
	            	'tahun_terbit' => $tahun_terbit,
	            	'tempat_terbit' => $tempat_terbit,
	            	'subyek' => $subyek,
	            	'deskripsi_fisik' => $deskripsi_fisik,
	            	'letak_buku' => $letak_buku,
	            	'thumbnail' => 'thumbnail-buku/'.$result['file_name'],
	            	'alamat_thumbnail' => $result['full_path']
	            );
	            $this->db->where('id_buku', $id);
	            $this->db->update('buku_tables', $data);
	            redirect(base_url('operator/data-buku'));
	        }
		}
	}
	//CRUD BUKU

	/** 
	====================================================================================================================	
	**/

	//CRUD LAPORAN
	function add_laporan(){
		$judul = $this->input->post('judul');
		$tahun = $this->input->post('tahun');
		$jenis = $this->input->post('jenis');
		$jurusan = $this->input->post('jurusan');
		$abstrak = $this->input->post('abstrak');
		$kata_kunci = $this->input->post('kata_kunci');

		$judul_replace = str_replace(" ", "-", $judul);
		$jenis_replace = str_replace(" ", "-", $jenis);
		$jurusan_replace = str_replace(" ", "-", $jurusan);

		$direktori_upload;
		$thumbnail;
		if ($jenis == 'Proyek 1') {
			$direktori_upload = 'proyek1/';
		} elseif ($jenis == 'Proyek 2') {
			$direktori_upload = 'proyek2/';
		} elseif ($jenis == 'Prakerin') {
			$direktori_upload = 'prakerin/';
		} elseif ($jenis == 'TPPA') {
			$direktori_upload = 'tppa/';
		} elseif ($jenis == 'Proyek Akhir') {
			$direktori_upload = 'proyek-akhir/';
		}

		if ($jurusan == 'Akuntansi') {
			$thumbnail = 'thumbnail-laporan/ak.png';
		} elseif ($jurusan == 'Teknik Informatika') {
			$thumbnail = 'thumbnail-laporan/ti.png';
		} elseif ($jurusan == 'PPM') {
			$thumbnail = 'thumbnail-laporan/mesin.png';
		}

		$config = array(
			'encrypt_name' => TRUE,
			'upload_path' => "./laporan/".$direktori_upload,
			'allowed_types' => 'doc|docx|pdf'
		);

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('berkas')) {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('error', $error);
            redirect(base_url('operator/add/laporan'));
        } else {
            $result = $this->upload->data();
            $data = array(
            	'judul_laporan' => $judul,
            	'tahun' => $tahun,
            	'abstrak' => $abstrak,
            	'kata_kunci' => $kata_kunci,
            	'jenis_laporan' => $jenis,
            	'alamat_file_laporan' => $result['full_path'],
            	'alamat_file_laporan_public' => "laporan/".$direktori_upload.$result['file_name'],
            	'nama_file' => $result['file_name'],
            	'jurusan' => $jurusan,
            	'thumbnail' => $thumbnail
            );
            $this->db->insert('laporan_tables', $data);
            if ($this->input->post('btn_submit') == 1) {
				redirect(base_url('operator/data-laporan'));
			} else if ($this->input->post('btn_submit') == 2) {
				redirect(base_url('operator/add/laporan'));
			}
        }
	}

	function edit_laporan($id){
		$judul = $this->input->post('judul');
		$tahun = $this->input->post('tahun');
		$jenis = $this->input->post('jenis');
		$jurusan = $this->input->post('jurusan');
		$abstrak = $this->input->post('abstrak');
		$kata_kunci = $this->input->post('kata_kunci');

		$berkas_baru = $_FILES['berkas']['name'];
		$berkas_lama = $this->input->post('berkas_lama');
		$nama_file = $this->input->post('nama_file');

		$judul_replace = str_replace(" ", "-", $judul);
		$jenis_replace = str_replace(" ", "-", $jenis);
		$jurusan_replace = str_replace(" ", "-", $jurusan);

		$direktori_upload;
		$thumbnail;
		if ($jenis == 'Proyek 1') {
			$direktori_upload = 'proyek1';
		} elseif ($jenis == 'Proyek 2') {
			$direktori_upload = 'proyek2';
		} elseif ($jenis == 'Prakerin') {
			$direktori_upload = 'prakerin';
		} elseif ($jenis == 'TPPA') {
			$direktori_upload = 'tppa';
		} elseif ($jenis == 'Proyek Akhir') {
			$direktori_upload = 'proyek-akhir';
		}

		if ($jurusan == 'Akuntansi') {
			$thumbnail = 'thumbnail-laporan/ak.png';
		} elseif ($jurusan == 'Teknik Informatika') {
			$thumbnail = 'thumbnail-laporan/ti.png';
		} elseif ($jurusan == 'PPM') {
			$thumbnail = 'thumbnail-laporan/mesin.png';
		}

		if ($berkas_baru == "") {
			$data = array(
            	'judul_laporan' => $judul,
            	'tahun' => $tahun,
            	'abstrak' => $abstrak,
            	'kata_kunci' => $kata_kunci,
            	'jenis_laporan' => $jenis,
            	'alamat_file_laporan' => $berkas_lama,
            	'nama_file' => $nama_file,
            	'jurusan' => $jurusan,
          		'thumbnail' => $thumbnail
            );
            $this->db->where('id_laporan', $id);
            $this->db->update('laporan_tables', $data);
            redirect(base_url('operator/data-laporan'));
		} else {
			$file_name = $jenis_replace."-".$judul_replace."-".$tahun."-".$jurusan_replace;
			$config = array(
				'file_name' => $file_name,
				'upload_path' => "./laporan/".$direktori_upload,
				'allowed_types' => 'doc|docx|pdf'
			);
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('berkas')) {
	            $error = $this->upload->display_errors();
	            $this->session->set_flashdata('error', $error);
	            redirect(base_url('operator/edit/laporan/'.$id));
	        } else {
	            $result = $this->upload->data();
	            $data = array(
	            	'judul_laporan' => $judul,
	            	'tahun' => $tahun,
	            	'abstrak' => $abstrak,
	            	'kata_kunci' => $kata_kunci,
	            	'jenis_laporan' => $jenis,
	            	'alamat_file_laporan' => $result['full_path'],
            		'alamat_file_laporan_public' => "laporan/".$direktori_upload.$result['file_name'],
	            	'nama_file' => $result['file_name'],
	            	'jurusan' => $jurusan,
            		'thumbnail' => $thumbnail
	            );
	            $this->db->where('id_laporan', $id);
	            $this->db->update('laporan_tables', $data);
	            redirect(base_url('operator/data-laporan'));
	        }
		}
	}
	//CRUD LAPORAN


	//=================


	//CRUD STOK BUKU

	function show_buku(){
		$query = $this->db->query(
			"SELECT id_buku, judul_buku
			FROM buku_tables
			ORDER BY id_buku DESC"
		);
		return $query->result_array();
	}

	function show_jumlah_buku(){
		$query = $this->db->query(
			"SELECT id_buku, COUNT(unique_id_buku) as jumlah_buku
			FROM stok_buku_tables
			where is_available = 'true'
			GROUP BY id_buku"
		);
		return $query->result_array();
	}

	function add_stok(){
		$buku = $this->input->post('buku');
		$unique_id_buku = $this->input->post('unique_id_buku');
		$data = array(
			'id_buku' => $buku,
			'unique_id_buku' => $unique_id_buku,
			'is_available' => 'true'
		);

		$this->db->insert('stok_buku_tables', $data);
		if ($this->input->post('btn_submit') == 1) {
			redirect(base_url('operator/data-stok-buku'));
		} else if ($this->input->post('btn_submit') == 2) {
			redirect(base_url('operator/add/stok_buku'));
		}
	}

	function delete_stok_buku($id_buku, $id_stok){
		$this->db->delete('stok_buku_tables', array('id_stok' => $id_stok));
		$this->db->delete('pemesanan_tables', array('id_stok' => $id_stok));
		$this->db->delete('transaksi_tables', array('id_stok' => $id_stok));
		$this->db->delete('log_tables', array('id_stok' => $id_stok));
		redirect(base_url('operator/view/stok_buku/'.$id_buku));
	}


	//CRUD STOK BUKU

	//==========================================================================================================


	//CRUD PEMESANAN

	function show_pemesanan(){
		$query = $this->db->query(
 			"SELECT pemesanan_tables.id_pemesanan, pemesanan_tables.nomor_pemesanan_unik, pemesanan_tables.tgl_pemesanan,
 					pemesanan_tables.pemesanan_dimulai_pada_jam, pemesanan_tables.pemesanan_hangus_pada_jam, 
 					pemesanan_tables.status, pemesanan_tables.nim, pemesanan_tables.id_stok, stok_buku_tables.unique_id_buku,
 					buku_tables.judul_buku, buku_tables.thumbnail,
 					mahasiswa_users.nama
 			 
 			 FROM pemesanan_tables
 			 
 			 LEFT JOIN stok_buku_tables ON stok_buku_tables.id_stok = pemesanan_tables.id_stok
 			 LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
 			 LEFT JOIN mahasiswa_users on pemesanan_tables.nim = mahasiswa_users.nim

 			 WHERE buku_tables.id_buku = stok_buku_tables.id_buku
 			 AND stok_buku_tables.id_stok = pemesanan_tables.id_stok
 			 ORDER BY id_pemesanan DESC
 			 "
 		);
 		return $query;
	}

	//============================================================================================================



	//CRUD PEMINJAMAN BUKU
	function show_all_peminjaman(){
		$query = $this->db->query(
			"SELECT transaksi_tables.id_transaksi, transaksi_tables.tgl_transaksi, transaksi_tables.tgl_pinjam_buku,
					transaksi_tables.tgl_kembali_buku, transaksi_tables.tgl_buku_dikembalikan, transaksi_tables.status_transaksi,
					transaksi_tables.total_denda, stok_buku_tables.unique_id_buku,

					mahasiswa_users.nama,

					buku_tables.judul_buku
			FROM transaksi_tables

			LEFT JOIN mahasiswa_users ON mahasiswa_users.nim = transaksi_tables.nim
			LEFT JOIN stok_buku_tables ON transaksi_tables.id_stok = stok_buku_tables.id_stok
			LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
			ORDER BY id_transaksi DESC"
		);

		return $query;
	}

	function get_stok_buku(){
		$query = $this->db->query(
			"SELECT buku_tables.judul_buku, 
					stok_buku_tables.unique_id_buku, stok_buku_tables.id_stok

			FROM stok_buku_tables

			LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku

			WHERE stok_buku_tables.is_available = 'true'"
		);

		return $query->result_array();
	}

	function get_mahasiswa(){
		$query = $this->db->query(
			"SELECT *
			FROM mahasiswa_users"
		);

		return $query->result_array();
	}

	function add_peminjaman(){
		//LOGICAL
		//1. CEK MAX PINJAM BUKU MELALUI CONFIG
		//2. CEK TOTAL TRANSAKSI PEMINJAMAN MAHASISWA, JIKA MASIH DIBAWAH MAX PINJAM CONFIG MAKA LANJUT
		//3. INSERT KE TABEL TRANSAKSI
		//4. UPDATE IS_AVAILABLE STOK BUKU MENJADI FALSE
		$date_now = date('Y-m-d');
		$tgl_transaksi = $date_now;
		//========
		$tgl_kembali_buku = date('Y-m-d', strtotime($date_now. '+7 days'));
		$tgl_kembali_buku_nama_hari = date('l', strtotime($date_now.$tgl_kembali_buku));
		//=========
		$nim = $this->input->post('nim', TRUE);
		$id_stok = $this->input->post('id_stok');

		//cek hari apakah hari pengembalian buku pada hari sabtu atau minggu
		if ($tgl_kembali_buku_nama_hari == 'Saturday' || $tgl_kembali_buku_nama_hari == 'Sabtu') {
			$tgl_kembali_buku = date('Y-m-d', strtotime($date_now. '+9 days'));
		} elseif ($tgl_kembali_buku_nama_hari == 'Sunday' || $tgl_kembali_buku_nama_hari == 'Minggu') {
			$tgl_kembali_buku = date('Y-m-d', strtotime($tgl_kembali_buku. '+8 days'));
		}
		//cek hari apakah hari pengembalian buku pada hari sabtu atau minggu

		$query_cek_config_max_pinjam_buku_saat_ini = $this->db->query(
			"SELECT config_total_pinjam_mahasiswa
			 FROM config_tables
			 LIMIT 0,1
			"
		);

		$query_cek_total_peminjaman_buku_mahasiswa_yang_berlangsung = $this->db->query(
			"SELECT COUNT(id_transaksi) as total_peminjaman_mahasiswa_saat_ini
			 FROM transaksi_tables
			 WHERE status_transaksi = 'peminjaman_sedang_berlangsung'
			 AND nim = '$nim'
			"
		);

		foreach ($query_cek_config_max_pinjam_buku_saat_ini->result() as $data_config) {
			$max_pinjam_buku_saat_ini = $data_config->config_total_pinjam_mahasiswa;
			foreach ($query_cek_total_peminjaman_buku_mahasiswa_yang_berlangsung->result() as $data_total_peminjaman_yang_berlangsung) {
				$total_mahasiswa_pinjam_buku_saat_ini = $data_total_peminjaman_yang_berlangsung->total_peminjaman_mahasiswa_saat_ini;
				if ($total_mahasiswa_pinjam_buku_saat_ini < $max_pinjam_buku_saat_ini) {
					$data = array(
						'tgl_transaksi' => $tgl_transaksi,
						'tgl_pinjam_buku' => $date_now,
						'tgl_kembali_buku' => $tgl_kembali_buku,
						'tgl_buku_dikembalikan' => '',
						'nim' => $nim,
						'id_stok' => $id_stok,
						'status_transaksi' => 'peminjaman_sedang_berlangsung',
						'total_denda' => 0
					);

					$data_update_buku_is_available = array(
						'is_available' => 'false' 
					);

					$this->db->insert('transaksi_tables', $data);
					$this->api_log->add_log_type_2($nim, $id_stok);
					$this->db->where('id_stok', $id_stok);
					$this->db->update('stok_buku_tables', $data_update_buku_is_available);
					redirect(base_url('operator/data-peminjaman-buku'));
				} else if ($total_mahasiswa_pinjam_buku_saat_ini == $max_pinjam_buku_saat_ini) {
					$this->session->set_flashdata('error', "Mohon maaf kesempatan pinjam buku mahasiswa tersebut habis");
					redirect(base_url('operator/add/peminjaman_buku'));
				}
			}
		}




		// $cek_kesempatan_pinjam_buku_saat_ini;
 	// 	$query_cek__kesempatan_pinjam_buku_saat_ini = $this->db->query(
 	// 		"SELECT kesempatan_pinjam_buku
 	// 		FROM mahasiswa_users
 	// 		WHERE nim = $nim"
 	// 	);

		// foreach ($query_cek__kesempatan_pinjam_buku_saat_ini->result() as $data_cek_kesempatan) {
		// 	$cek_kesempatan_pinjam_buku_saat_ini = $data_cek_kesempatan->kesempatan_pinjam_buku;

		// 	if ($cek_kesempatan_pinjam_buku_saat_ini > 0) {
				
		// 		$kesempatan_pinjam_buku_saat_ini;
		//  		$query_get_kesempatan_pinjam_buku_saat_ini = $this->db->query(
		//  			"SELECT kesempatan_pinjam_buku
		//  			FROM mahasiswa_users
		//  			WHERE nim = $nim"
		//  		);

		//  		if ($query_get_kesempatan_pinjam_buku_saat_ini->num_rows() == 1) {
		//  			foreach ($query_get_kesempatan_pinjam_buku_saat_ini->result() as $data_kesempatan) {
		//  				$kesempatan_pinjam_buku_saat_ini = $data_kesempatan->kesempatan_pinjam_buku - 1;
		//  			}
		//  		}

		// 		$data_update_kesempatan_pinjam_buku_mahasiswa = array(
		//  			'kesempatan_pinjam_buku' => $kesempatan_pinjam_buku_saat_ini
		//  		);

		//  		$data = array(
		// 			'tgl_transaksi' => $tgl_transaksi,
		// 			'tgl_pinjam_buku' => $date_now,
		// 			'tgl_kembali_buku' => $tgl_kembali_buku,
		// 			'tgl_buku_dikembalikan' => '',
		// 			'nim' => $nim,
		// 			'id_stok' => $id_stok,
		// 			'status_transaksi' => 'peminjaman_sedang_berlangsung',
		// 			'total_denda' => 0
		// 		);

		// 		$data_update_buku_is_available = array(
		// 			'is_available' => 'false' 
		// 		);

		// 		$this->db->insert('transaksi_tables', $data);
		// 		$this->api_log->add_log_type_2($nim, $id_stok);
		// 		$this->db->where('id_stok', $id_stok);
		// 		$this->db->update('stok_buku_tables', $data_update_buku_is_available);

		// 		$this->db->where('nim', $nim);
		// 		$this->db->update('mahasiswa_users', $data_update_kesempatan_pinjam_buku_mahasiswa);
		// 		redirect(base_url('operator/data-peminjaman-buku'));
		// 	} else {
		// 		$this->session->set_flashdata('error', "Mohon maaf kesempatan pinjam buku mahasiswa tersebut habis");
		// 		redirect(base_url('operator/add/peminjaman_buku'));
		// 	}
		// }

	}

	function add_peminjaman_from_pemesanan($id_pemesanan, $nim, $id_stok){
		$date_now = date('Y-m-d');
		$tgl_transaksi = $date_now;
		//========
		$tgl_kembali_buku = date('Y-m-d', strtotime($date_now. '+7 days'));
		$tgl_kembali_buku_nama_hari = date('l', strtotime($date_now.$tgl_kembali_buku));
		//=========

		$query_cek_validitas_pemesanan = $this->db->query(
			"SELECT tgl_pemesanan, pemesanan_hangus_pada_jam
			FROM pemesanan_tables
			WHERE id_pemesanan = $id_pemesanan"
		);

		$value_tgl_pemesanan;
		$value_pemesanan_hangus_pada_jam;
		foreach ($query_cek_validitas_pemesanan->result() as $data_validitas_pemesanan) {
			$value_tgl_pemesanan = $data_validitas_pemesanan->tgl_pemesanan;
			$value_pemesanan_hangus_pada_jam = $data_validitas_pemesanan->pemesanan_hangus_pada_jam;

			if ($value_tgl_pemesanan == $date_now && date('H:i') <= $value_pemesanan_hangus_pada_jam) {
				//cek hari apakah hari pengembalian buku pada hari sabtu atau minggu
				if ($tgl_kembali_buku_nama_hari == 'Saturday' || $tgl_kembali_buku_nama_hari == 'Sabtu') {
					$tgl_kembali_buku = date('Y-m-d', strtotime($date_now. '+9 days'));
				} elseif ($tgl_kembali_buku_nama_hari == 'Sunday' || $tgl_kembali_buku_nama_hari == 'Minggu') {
					$tgl_kembali_buku = date('Y-m-d', strtotime($tgl_kembali_buku. '+8 days'));
				}
				//cek hari apakah hari pengembalian buku pada hari sabtu atau minggu

				$query_cek_config_max_pinjam_buku_saat_ini = $this->db->query(
					"SELECT config_total_pinjam_mahasiswa
					 FROM config_tables
					 LIMIT 0,1
					"
				);

				$query_cek_total_peminjaman_buku_mahasiswa_yang_berlangsung = $this->db->query(
					"SELECT COUNT(id_transaksi) as total_peminjaman_mahasiswa_saat_ini
					 FROM transaksi_tables
					 WHERE status_transaksi = 'peminjaman_sedang_berlangsung'
					 AND nim = '$nim'
					"
				);

				foreach ($query_cek_config_max_pinjam_buku_saat_ini->result() as $data_config) {
					$max_pinjam_buku_saat_ini = $data_config->config_total_pinjam_mahasiswa;
					foreach ($query_cek_total_peminjaman_buku_mahasiswa_yang_berlangsung->result() as $data_total_peminjaman_yang_berlangsung) {
						$total_mahasiswa_pinjam_buku_saat_ini = $data_total_peminjaman_yang_berlangsung->total_peminjaman_mahasiswa_saat_ini;
						if ($total_mahasiswa_pinjam_buku_saat_ini < $max_pinjam_buku_saat_ini) {
							$data = array(
								'tgl_transaksi' => $tgl_transaksi,
								'tgl_pinjam_buku' => $date_now,
								'tgl_kembali_buku' => $tgl_kembali_buku,
								'tgl_buku_dikembalikan' => '',
								'nim' => $nim,
								'id_stok' => $id_stok,
								'status_transaksi' => 'peminjaman_sedang_berlangsung',
								'total_denda' => 0
							);

							$data_update_buku_is_available = array(
								'is_available' => 'false' 
							);

							$data_update_status_pemesanan = array(
								'status' => 'success'
							);

							$this->db->insert('transaksi_tables', $data);
							$this->api_log->add_log_type_2($nim, $id_stok);

							$this->db->where('id_stok', $id_stok);
							$this->db->update('stok_buku_tables', $data_update_buku_is_available);

							$this->db->where('id_pemesanan', $id_pemesanan);
							$this->db->update('pemesanan_tables', $data_update_status_pemesanan);
							$this->session->set_flashdata('success', "Peminjaman baru berhasil ditambahkan");
							redirect(base_url('operator/data-pemesanan-buku'));
						} else if ($total_mahasiswa_pinjam_buku_saat_ini == $max_pinjam_buku_saat_ini) {
							$data_update_buku_is_available = array(
								'is_available' => 'true'  
							);

							$data_update_status_pemesanan = array(
								'status' => 'failed'
							);

							$this->db->where('id_stok', $id_stok);
							$this->db->update('stok_buku_tables', $data_update_buku_is_available);

							$this->db->where('id_pemesanan', $id_pemesanan);
							$this->db->update('pemesanan_tables', $data_update_status_pemesanan);

							$this->session->set_flashdata('error', "Mohon maaf kesempatan pinjam buku mahasiswa tersebut habis");
							redirect(base_url('operator/data-pemesanan-buku'));
						}
					}
				}


				// $cek_kesempatan_pinjam_buku_saat_ini;
		 	// 	$query_cek__kesempatan_pinjam_buku_saat_ini = $this->db->query(
		 	// 		"SELECT kesempatan_pinjam_buku
		 	// 		FROM mahasiswa_users
		 	// 		WHERE nim = $nim"
		 	// 	);

		 	// 	foreach ($query_cek__kesempatan_pinjam_buku_saat_ini->result() as $data_cek_kesempatan) {
				// 	$cek_kesempatan_pinjam_buku_saat_ini = $data_cek_kesempatan->kesempatan_pinjam_buku;

				// 	if ($cek_kesempatan_pinjam_buku_saat_ini > 0) {
						
				// 		$kesempatan_pinjam_buku_saat_ini;
				//  		$query_get_kesempatan_pinjam_buku_saat_ini = $this->db->query(
				//  			"SELECT kesempatan_pinjam_buku
				//  			FROM mahasiswa_users
				//  			WHERE nim = $nim"
				//  		);

				//  		if ($query_get_kesempatan_pinjam_buku_saat_ini->num_rows() == 1) {
				//  			foreach ($query_get_kesempatan_pinjam_buku_saat_ini->result() as $data_kesempatan) {
				//  				$kesempatan_pinjam_buku_saat_ini = $data_kesempatan->kesempatan_pinjam_buku - 1;
				//  			}
				//  		}

				// 		$data_update_kesempatan_pinjam_buku_mahasiswa = array(
				//  			'kesempatan_pinjam_buku' => $kesempatan_pinjam_buku_saat_ini
				//  		);

				//  		$data = array(
				// 			'tgl_transaksi' => $tgl_transaksi,
				// 			'tgl_pinjam_buku' => $date_now,
				// 			'tgl_kembali_buku' => $tgl_kembali_buku,
				// 			'tgl_buku_dikembalikan' => '',
				// 			'nim' => $nim,
				// 			'id_stok' => $id_stok,
				// 			'status_transaksi' => 'peminjaman_sedang_berlangsung',
				// 			'total_denda' => 0
				// 		);

				// 		$data_update_buku_is_available = array(
				// 			'is_available' => 'false' 
				// 		);

				// 		$data_update_status_pemesanan = array(
				// 			'status' => 'success'
				// 		);

				// 		$this->db->insert('transaksi_tables', $data);
				// 		$this->api_log->add_log_type_2($nim, $id_stok);

				// 		$this->db->where('id_stok', $id_stok);
				// 		$this->db->update('stok_buku_tables', $data_update_buku_is_available);

				// 		$this->db->where('nim', $nim);
				// 		$this->db->update('mahasiswa_users', $data_update_kesempatan_pinjam_buku_mahasiswa);

				// 		$this->db->where('id_pemesanan', $id_pemesanan);
				// 		$this->db->update('pemesanan_tables', $data_update_status_pemesanan);
				// 		$this->session->set_flashdata('success', "Peminjaman baru berhasil ditambahkan");
				// 		redirect(base_url('operator/data-pemesanan-buku'));
				// 	} else {
				// 		$data_update_buku_is_available = array(
				// 			'is_available' => 'true'  
				// 		);

				// 		$data_update_status_pemesanan = array(
				// 			'status' => 'failed'
				// 		);

				// 		$this->db->where('id_stok', $id_stok);
				// 		$this->db->update('stok_buku_tables', $data_update_buku_is_available);

				// 		$this->db->where('id_pemesanan', $id_pemesanan);
				// 		$this->db->update('pemesanan_tables', $data_update_status_pemesanan);

				// 		$this->session->set_flashdata('error', "Mohon maaf kesempatan pinjam buku mahasiswa tersebut habis");
				// 		redirect(base_url('operator/data-pemesanan-buku'));
				// 	}
				// }
			} else {
				$this->session->set_flashdata('error', "Waktu pemesanan buku telah kadaluwarsa. Status pemesanan telah diubah menjadi kadaluwarsa");
				$data_update_status_pemesanan = array(
					'status' => 'expired'
				);
				$data_update_buku_is_available = array(
							'is_available' => 'true'  
						);

				$this->db->where('id_pemesanan', $id_pemesanan);
				$this->db->update('pemesanan_tables', $data_update_status_pemesanan);

				$this->db->where('id_stok', $id_stok);
				$this->db->update('stok_buku_tables', $data_update_buku_is_available);

				redirect(base_url('operator/data-pemesanan-buku'));
			}
		}

	}

	function pengembalian_buku($id_transaksi){
		$tgl_kembali;
		$tgl_buku_dikembalikan = new DateTime(date('Y-m-d'));
		$nominal_denda;
		$total_denda;
		$query = $this->db->query(
			"SELECT * 
			 FROM transaksi_tables
			 WHERE id_transaksi = $id_transaksi
			 LIMIT 0,1"
		);

		$query_get_nominal_denda = $this->db->get('config_tables')->result();
		foreach ($query_get_nominal_denda as $data_denda) {
			$nominal_denda = $data_denda->nominal_denda;
		}

		if ($query->num_rows() == 1) {
			foreach ($query->result() as $data) {
				$tgl_kembali = new DateTime($data->tgl_kembali_buku);
				$interval = $tgl_kembali->diff($tgl_buku_dikembalikan);
				$total_hari_tanpa_weekend;
				$id_stok = $data->id_stok;
				$nim = $data->nim;
				if ($tgl_buku_dikembalikan > $tgl_kembali) {
					for($i=0; $i<=$interval->d; $i++){
					    $modif = $tgl_kembali->modify('+1 day');
					    $weekday = $tgl_kembali->format('w');

					    if($weekday != 0 && $weekday != 6){ // 0 for Sunday and 6 for Saturday
					        $total_hari_tanpa_weekend++;  
					    }
					}
					// echo $total_hari_tanpa_weekend." days without weekend";
					$denda = ($nominal_denda * $total_hari_tanpa_weekend) - $nominal_denda;
					$data_update_pengembalian_buku = array(
						'total_denda' => $denda,
						'tgl_buku_dikembalikan' => date("Y-m-d"),
						'status_transaksi' => "peminjaman_telah_berakhir"
					);
					$data_update_buku_is_available = array(
						'is_available' => 'true'  
					);

					$this->db->where("id_transaksi", $id_transaksi);
					$this->db->update("transaksi_tables", $data_update_pengembalian_buku);
					$this->api_log->add_log_type_3($nim, $id_stok);

					$this->db->where("id_stok", $id_stok);
					$this->db->update("stok_buku_tables", $data_update_buku_is_available);

					$this->session->set_flashdata('error', "Peminjaman telah berakhir dengan total denda sebanyak Rp." . $denda);

				} else if ($tgl_buku_dikembalikan <= $tgl_kembali) {

					$data_update_pengembalian_buku = array(
						'total_denda' => 0,
						'tgl_buku_dikembalikan' => date("Y-m-d"),
						'status_transaksi' => "peminjaman_telah_berakhir"
					);
					$data_update_buku_is_available = array(
						'is_available' => 'true'  
					);

					$this->db->where("id_transaksi", $id_transaksi);
					$this->db->update("transaksi_tables", $data_update_pengembalian_buku);
					$this->api_log->add_log_type_3($nim, $id_stok);

					$this->db->where("id_stok", $id_stok);
					$this->db->update("stok_buku_tables", $data_update_buku_is_available);

					$this->session->set_flashdata('success', "Peminjaman telah berakhir tanpa denda");
				}
				
				$kesempatan_pinjam_buku_saat_ini;
		 		$query_get_kesempatan_pinjam_buku_saat_ini = $this->db->query(
		 			"SELECT kesempatan_pinjam_buku
		 			FROM mahasiswa_users
		 			WHERE nim = $nim"
		 		);

		 		if ($query_get_kesempatan_pinjam_buku_saat_ini->num_rows() == 1) {
		 			foreach ($query_get_kesempatan_pinjam_buku_saat_ini->result() as $data_kesempatan) {
		 				$kesempatan_pinjam_buku_saat_ini = $data_kesempatan->kesempatan_pinjam_buku + 1;
		 			}
		 		}

				$data_update_kesempatan_pinjam_buku_mahasiswa = array(
		 			'kesempatan_pinjam_buku' => $kesempatan_pinjam_buku_saat_ini
		 		);

		 		$this->db->where('nim', $nim);
				$this->db->update('mahasiswa_users', $data_update_kesempatan_pinjam_buku_mahasiswa);
				redirect(base_url('operator/data-peminjaman-buku/'));
			}
		}
	}

	//============================================================================================================
}
 ?>