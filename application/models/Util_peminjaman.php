<?php 

/**
* 
*/
class Util_peminjaman extends CI_Model{
	
	function __construct(){
		parent::__construct();
 		date_default_timezone_set('Asia/Jakarta');
	}

	function check_status_transaksi(){
		$date_now = date('Y-m-d');
		$query = $this->db->query(
			"SELECT *
			 FROM transaksi_tables
			 WHERE tgl_kembali_buku < '$date_now'
             AND status_transaksi = 'peminjaman_sedang_berlangsung'
			"
		);
		if ($query->num_rows() > 0 ) {
			foreach ($query->result() as $data) {
				$id_transaksi = $data->id_transaksi;

				$data_update_status_transaksi = array(
					'status_transaksi' => 'peminjaman_melewati_batas' 
				);
				$this->db->where('id_transaksi', $id_transaksi);
				$this->db->update('transaksi_tables', $data_update_status_transaksi);
			}
		}
	}

}

 ?>