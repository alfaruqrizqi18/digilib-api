<?php 
/**
 * 
 */
 class Api_mahasiswa extends CI_Model{
 	
 	function __construct(){
 		parent::__construct();
 	}

 	function get_mahasiswa(){
		$this->db->select('id_mahasiswa, nim, nama, jurusan, token_android, kesempatan_pesan_buku');
		$this->db->order_by('id_mahasiswa', 'DESC');
		$query = $this->db->get('mahasiswa_users')->result_array();
		$return = $this->output->set_output(
		json_encode(
			array(
				'result' => $query,
				'status' => 'OK'
			)
		)
		);
 		return $return;
 	}

 	function post_mahasiswa(){
 		if ($this->input->server('REQUEST_METHOD') == 'POST') {
 			$owner = $this->input->post('owner');
	 		$nim = $this->input->post('nim');
	 		$password = $this->input->post('password');
	 		$data = array(
	 			'owner' => $owner,
	 			'nim' => $nim,
	 			'password' => $password,
	 			'created_at' => date('d M Y')
	 		);
	 		$post = $this->db->insert('lb_user', $data);

	 		if ($post) {
	 			$result = array(
	 				'status' => 'Success',
	 				'data' => $data
	 			);
	 		} else {
	 			$result = array(
	 				'status' => 'Failed'
	 			);
	 		}
	 		$return = $this->output->set_output(
	 			json_encode(
	 				array('result' => $result)
	 			)
	 		);
 		} else {
 			$result = array(
	 				'status' => 'Metode yang kamu gunakan tidak diizinkan'
	 		);
 			$return = $this->output->set_output(
	 			json_encode(
	 				array('result' => $result)
	 			)
	 		);
 		}
 		return $return;
 	}

 	function update_mahasiswa(){
 		if ($this->input->server('REQUEST_METHOD') == 'POST') {
 			$id_user = $this->input->post('id_user');
	 		$owner = $this->input->post('owner');
	 		$nim = $this->input->post('nim');
	 		$password = $this->input->post('password');
	 		$data = array(
	 			'owner' => $owner,
	 			'nim' => $nim,
	 			'password' => $password
	 		);
	 		$checking = $this->db->get_where('lb_user',array('id_user' => $id_user));

	 		if (count($checking) == 1) {
		 		$update = $this->db->where('id_user', $id_user);
		 		$update = $this->db->update('lb_user', $data);
		 			if ($update) {
		 				$result = array(
			 				'status' => 'Success',
			 				'id_user' => $id_user,
			 				'data' => $data
	 					);
		 			}

	 		} else {

	 			$result = array(
	 				'status' => 'Failed'
	 			);
	 		}

	 		$return = $this->output->set_output(
	 			json_encode(
	 				array('result' => $result)
	 			)
	 		);

 		} else {

 			$result = array(
	 				'status' => 'Metode yang kamu gunakan tidak diizinkan'
	 		);
 			$return = $this->output->set_output(
	 			json_encode(
	 				array('result' => $result)
	 			)
	 		);
 		}
 		return $return;
 	}

 	function delete_mahasiswa(){
 		if ($this->input->server('REQUEST_METHOD') == 'POST') {
 			$id_user = $this->input->post('id_user');
	 		$data = array(
	 			'id_user' => $id_user
	 		);
	 		$checking = $this->db->get_where('lb_user', $data)->result();

	 		if (count($checking) == 1) {
	 			$delete = $this->db->where('id_user', $id_user);
	 			$delete = $this->db->delete('lb_user', $data);
	 			if ($delete) {
	 				$result = array(
		 				'status' => 'Success',
		 				'id_user' => $id_user,
		 				'data' => $data
	 				);
	 			}

	 		} else {

	 			$result = array(
	 				'status' => 'Gagal',
	 				'data' => 'ID Tidak tersedia.'
	 			);
	 		}

	 		$return = $this->output->set_output(
	 			json_encode(
	 				array('result' => $result)
	 			)
	 		);

 		} else {

 			$result = array(
	 				'status' => 'Metode yang kamu gunakan tidak diizinkan'
	 		);
 			$return = $this->output->set_output(
	 			json_encode(
	 				array('result' => $result)
	 			)
	 		);
 		}
 		return $return;
 	}

 } 

 ?>