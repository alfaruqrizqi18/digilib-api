<?php 

/**
* 
*/
class Api_log extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	function add_log_type_1($nim, $result_id_stok){ // Log untuk pemesanan buku dari android
 		$date_now = date('Y-m-d');
		// $query = $this->db->query(
		// 	"SELECT buku_tables.judul_buku
		// 	 FROM log_tables
		// 	 LEFT JOIN stok_buku_tables ON stok_buku_tables.id_stok = log_tables.id_stok
 	// 		 LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
 	// 		 WHERE log_tables.id_stok = '$result_id_stok'"
		// );
		// foreach ($query->result() as $data) {
			$data_log = array(
	 			'nim' => $nim,
	 			'judul_log' => 'Pemesanan Buku',
	 			'isi_log' => 'Kamu telah memesan satu buah buku dengan judul :',
	 			'id_stok' => $result_id_stok,
	 			'tgl_log' => $date_now,
	 			'type_log' => 1
	 		);
		// }
	 	$this->db->insert('log_tables', $data_log);
	}

	function add_log_type_2($nim, $id_stok){ // Log untuk peminjaman buku dari web
		$date_now = date('Y-m-d');
		$data_log = array(
 			'nim' => $nim,
 			'judul_log' => 'Transaksi Peminjaman Buku',
 			'isi_log' => 'Kamu telah melakukan satu transaksi peminjaman buku dengan judul :',
 			'id_stok' => $id_stok,
 			'tgl_log' => $date_now,
 			'type_log' => 2
 		);
	 	$this->db->insert('log_tables', $data_log);
	}

	function add_log_type_3($nim, $id_stok){ // Log untuk pengembalian buku dari web
		$date_now = date('Y-m-d');
		$data_log = array(
 			'nim' => $nim,
 			'judul_log' => 'Pengembalian Buku',
 			'isi_log' => 'Terima kasih, kamu telah mengembalikan buku dengan judul :',
 			'id_stok' => $id_stok,
 			'tgl_log' => $date_now,
 			'type_log' => 3
 		);
	 	$this->db->insert('log_tables', $data_log);
	}

}

 ?>