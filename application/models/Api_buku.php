<?php 

/**
* 
*/
class Api_buku extends CI_Model{
	
	function __construct(){

	}

	function get_all_buku() {
		$query = $this->db->get('buku_tables');
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function get_buku_for_landingpage_books(){
		$query = $this->db->query(
			"SELECT * 
			FROM buku_tables
			ORDER BY RAND()
			LIMIT 0,10"
		);
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function get_landingpage_full_filter($pengarang, $subyek, $isbn, $gmd) {
		$gmd_replace = str_replace('%20', ' ', $gmd);
		$query = $this->db->query(
			"SELECT * 
			FROM buku_tables
			WHERE pengarang LIKE '%$pengarang%'

			AND subyek LIKE '%$subyek%'

			AND isbn LIKE '%$isbn%'

			AND gmd LIKE '%$gmd%'
			
			");
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function full_filter($judul, $pengarang, $subyek, $isbn, $gmd) {
		$query = $this->db->query(
			"SELECT * 
			FROM buku_tables
			WHERE judul_buku LIKE '%$judul%'

			AND pengarang LIKE '%$pengarang%'

			AND subyek LIKE '%$subyek%'

			AND isbn LIKE '%$isbn%'

			AND gmd LIKE '%$gmd%'
			
			");
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function get_buku_by_param($param) {
		$query = $this->db->query(
			"SELECT * 
			FROM buku_tables
			WHERE 
			judul_buku LIKE '%$param'
			OR judul_buku LIKE '%$param%'
			OR judul_buku LIKE '$param%'

			OR subyek LIKE '%$param'
			OR subyek LIKE '%$param%'
			OR subyek LIKE '$param%'

			OR jurusan LIKE '%$param'
			OR jurusan LIKE '%$param%'
			OR jurusan LIKE '$param%'");
		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query->num_rows(),
				'status' => 'OK',
				'results' => $query->result_array()
			)
		)
		);
		return $return;
	}

	function get_stock($id_buku){
		$query_buku = $this->db->query(
			"SELECT *
			FROM buku_tables
			WHERE id_buku = '$id_buku'"
		);

		//====================
		$query_stok_buku = $this->db->query(
			"SELECT COUNT(unique_id_buku) as stock
			FROM stok_buku_tables
			WHERE id_buku = '$id_buku'
			AND is_available = 'true'")->result();

		foreach ($query_stok_buku as $data_stok) {
			$stok = $data_stok->stock;
		}
		//====================

		$query_show_detail_stok = $this->db->query(
			"SELECT *
			FROM stok_buku_tables
			WHERE id_buku = '$id_buku'
			AND is_available = 'true'");


		$return = $this->output->set_output(
		json_encode(
			array(
				'rows' => $query_buku->num_rows(),
				'status' => 'OK',
				'total_stock' => $stok,
				'detail_stock' =>$query_show_detail_stok->result_array(),
				'results' => $query_buku->result_array()
			)
		)
		);
	}

}
 ?>