<?php 

/**
* 
*/
class Auth_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	function profile($nim){
		$query_counter = $this->db->query(
			"SELECT 
			(SELECT count(*)
			 FROM pemesanan_tables
			 WHERE nim = '$nim') as total_pemesanan,

			(SELECT count(*)
			 FROM transaksi_tables
			 WHERE nim = '$nim') as total_transaksi,
			 
			(SELECT SUM(total_denda)
			 FROM transaksi_tables
			 WHERE nim = '$nim') as total_denda
			"
		);
		$query_log = $this->db->query(
			"SELECT log_tables.id_log,log_tables.judul_log, log_tables.isi_log, log_tables.id_stok, 
					log_tables.type_log, log_tables.tgl_log,
					buku_tables.judul_buku
			FROM log_tables
			LEFT JOIN stok_buku_tables ON stok_buku_tables.id_stok = log_tables.id_stok
			LEFT JOIN buku_tables ON buku_tables.id_buku = stok_buku_tables.id_buku
			WHERE log_tables.nim = '$nim'

			ORDER BY log_tables.id_log DESC"
		);
		$return = $this->output->set_output(
		json_encode(
			array(
				'status' => 'OK',
				'results_counter' => $query_counter->result(),
				'results_log' => $query_log->result()
			)
		)
		);
		return $return;
	}

	function admin(){
		$username = $this->input->post("username", TRUE);
		$password = $this->input->post("password", TRUE);

		if ($username != null && $password != null) {
			$query = $this->db->query(
				"SELECT *
				 FROM admin_users
				 WHERE BINARY username = '$username'
				 AND BINARY password = '$password'
				");
			if ($query->num_rows() == 1) {
				foreach ($query->result() as $session) {
					$session_data = array(
						'digilib_username' => $session->username,
						'digilib_password' => $session->password,
						'digilib_nama' => $session->nama,
						'digilib_user_level' => $session->level 
					);
					$this->session->set_userdata($session_data);
					if ($session->level == 'Admin') {
						redirect(base_url('admin'));
					} else if ($session->level == 'Operator'){
						redirect(base_url('operator'));
					}
				}
			} else {
				$this->session->set_flashdata('pesan_peringatan','Username atau password yang Anda masukkan salah');
				redirect(base_url('auth'));
			}
		} else {
			$this->session->set_flashdata('pesan_peringatan','Mohon isi username dan password');
			redirect(base_url('auth'));
		}
	}

	function mahasiswa(){
		$nim = $this->input->post('nim', TRUE);
		$password = $this->input->post('password', TRUE);
		$token = $this->input->post('token', TRUE);

		if ($nim != null && $password != null) {
			$query = $this->db->query(
				"SELECT *
				 FROM mahasiswa_users
				 WHERE BINARY nim = '$nim'
				 AND BINARY password = '$password'
				");
			if ($query->num_rows() == 1) {
				$return = $this->output->set_output(
				json_encode(
					array(
						'results' => $query->result_array(),
						'status' => 'OK'
					)
				)
				);
				$this->updateToken($nim, $token);
			} else {
				$return = $this->output->set_output(
				json_encode(
					array(
						'results' => [],
						'status' => 'NULL'
					)
				)
				);
			}
			return $return;
		}
	}

	function updateToken($nim, $token){
		$data = array(
			'token_android' => $token
		);
		$this->db->where('nim', $nim);
		$this->db->update('mahasiswa_users', $data);
	}

	function change_password(){
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$nim = $this->input->post('nim');
			$password_baru = $this->input->post('password_baru');

			$data = array(
				'password' => $password_baru 
			);
			$update = $this->db->where('nim', $nim);
			$update = $this->db->update('mahasiswa_users', $data);

			$query = $this->db->query(
				"SELECT *
				 FROM mahasiswa_users
				 WHERE nim = '$nim'
				 AND password = '$password_baru'"
			);

			$return = $this->output->set_output(
				json_encode(
					array(
						'status' => 'OK',
						'results' => $query->result_array()
					)
				)
			);
		 	
		 	return $return;
		}
	}

	function logout(){
		$this->session->unset_userdata('digilib_username');
		$this->session->unset_userdata('digilib_password');
		$this->session->unset_userdata('digilib_nama');
		$this->session->unset_userdata('digilib_user_level');
		session_destroy();
		redirect(base_url('auth'));
	}
}
?>