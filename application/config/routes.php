<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route = array(
	'default_controller' => 'frontend',
	'admin/data-admin-operator' => 'admin/data_admin_operator',
	'admin/data-mahasiswa' => 'admin/data_mahasiswa',
	'operator/data-buku' => 'operator/data_buku',
	'operator/data-stok-buku' => 'operator/data_stok_buku',
	'operator/data-laporan' => 'operator/data_laporan',
	'operator/data-peminjaman-buku' => 'operator/data_peminjaman_buku',
	'operator/data-pemesanan-buku' => 'operator/data_pemesanan_buku',
	'operator/my-profile' => 'operator/my_profile/'
);
$route['404_override'] = 'pagenotfound';
$route['translate_uri_dashes'] = FALSE;
