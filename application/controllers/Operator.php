<?php 
/**
 * 
 */
 class Operator extends CI_Controller {
 	
 	function __construct(){
 		parent::__construct();
 		if ($this->session->userdata('digilib_user_level') == null) {
 			redirect(base_url('auth'));
 		} else if ($this->session->userdata('digilib_user_level') == 'Admin') {
 			redirect(base_url('admin'));
 		} else {
 			$this->load->model('operator_model');
 		}
 	}

 	function index() {
 		$this->load->view('operator/header');
 		$this->load->view('operator/index');
 	}

 	function data_buku() {
 		$data = array(
 			'data_buku' => $this->operator_model->show('buku')
 		);
 		$this->load->view('operator/header');
 		$this->load->view('operator/data-buku', $data);
 	}

 	function data_stok_buku() {
 		$data = array(
 			'show_buku' => $this->operator_model->show_buku(),
 			'show_jumlah_buku' => $this->operator_model->show_jumlah_buku(),
 		);
 		$this->load->view('operator/header');
 		$this->load->view('operator/data-stok-buku', $data);
 	}

 	function data_laporan() {
 		$data = array(
 			'data_laporan' => $this->operator_model->show('laporan')
 		);
 		$this->load->view('operator/header');
 		$this->load->view('operator/data-laporan', $data);
 	}

 	function data_pemesanan_buku() {
 		$data = array(
 			'data_pemesanan' => $this->operator_model->show('pemesanan_buku')  
 		);
 		$this->load->view('operator/header');
 		$this->load->view('operator/data-pemesanan-buku', $data);
 	}

 	function data_peminjaman_buku() {
 		$data = array(
 			'data_peminjaman_buku' => $this->operator_model->show('peminjaman_buku')  
 		);
 		$this->load->view('operator/header');
 		$this->load->view('operator/data-peminjaman-buku', $data);
 	}

 	function my_profile() {

 	}

 	// CRUD OPERATIONS
 	function view($param, $id) {
 		if ($param == 'buku') {
 			$data = array(
 				'data_buku' => $this->operator_model->show_by_id($param, $id) 
 			);
 			$this->load->view('operator/header');
 			$this->load->view('operator/form/form-view-buku', $data);
 		} else if ($param == 'stok_buku') {
 			$data = array(
 				'data_stok_buku' => $this->operator_model->show_by_id($param, $id) 
 			);
 			$this->load->view('operator/header');
 			$this->load->view('operator/view-stok-buku', $data);
 		}
 	}

 	function add($param) {
 		if ($param == 'buku') {

 			if ($this->input->server('REQUEST_METHOD') == 'POST') {
 				$this->operator_model->add($param);
 			} else {
 				$this->load->view('operator/header');
 				$this->load->view('operator/form/form-add-buku');
 			}

 		} else if ($param == 'laporan') {

 			if ($this->input->server('REQUEST_METHOD') == 'POST') {
 				$this->operator_model->add($param);
 			} else {
 				$this->load->view('operator/header');
 				$this->load->view('operator/form/form-add-laporan');
 			}

 		} else if ($param == 'peminjaman_buku') {

 			if ($this->input->server('REQUEST_METHOD') == 'POST') {
 				$this->operator_model->add($param);
 			} else {
 				$data = array(
 					'data_stok_buku' => $this->operator_model->get_stok_buku(),
 					'data_mahasiswa' => $this->operator_model->get_mahasiswa() 
 				);
 				$this->load->view('operator/header');
 				$this->load->view('operator/form/form-add-peminjaman', $data);
 			}

 		} else if ($param == 'stok_buku') {

 			if ($this->input->server('REQUEST_METHOD') == 'POST') {
 				$this->operator_model->add($param);
 			} else {
 				$data = array(
 					'show_buku' => $this->operator_model->show_buku()
 				);
 				$this->load->view('operator/header');
 				$this->load->view('operator/form/form-add-stok', $data);
 			}

 		}
 	}

 	function add_peminjaman_from_pemesanan($id_pemesanan, $nim, $id_stok) {
		$this->operator_model->add_peminjaman_from_pemesanan($id_pemesanan, $nim, $id_stok);
 	}

 	function pengembalian_buku($id_transaksi){
 		$this->operator_model->pengembalian_buku($id_transaksi);
 	}

 	function edit($param, $id) {
 		if ($param == 'buku') {
 			if ($this->input->server("REQUEST_METHOD") == 'POST') {
 				$this->operator_model->edit($param, $id);
 			} else {
 			$data = array(
 				'data_buku' => $this->operator_model->show_by_id($param, $id) 
 			);
 			$this->load->view('operator/header');
 			$this->load->view('operator/form/form-edit-buku', $data);
 			}
 		} elseif ($param == 'laporan') {
 			if ($this->input->server("REQUEST_METHOD") == 'POST') {
 				$this->operator_model->edit($param, $id);
 			} else {
 				$data = array(
 					'data_laporan' => $this->operator_model->show_by_id($param, $id));
 				$this->load->view('operator/header');
 				$this->load->view('operator/form/form-edit-laporan', $data);
 			}
 		} elseif ($param == 'pemesanan_buku') {

 		} elseif ($param == 'peminjaman_buku') {
 			# code...
 		}
 	}

 	function delete($param, $id, $url_file_path_for_laporan) {
 		$this->operator_model->delete($param, $id, $url_file_path_for_laporan);
 	}

 	function delete_stok_buku($id_buku, $id_stok){
 		$this->operator_model->delete_stok_buku($id_buku, $id_stok);
 	}
	// CRUD OPERATIONS 	


 	function pengaturan(){
 		if ($this->input->server("REQUEST_METHOD") == 'POST') {
 			$max_peminjaman = $this->input->post('max_peminjaman');
 			$waktu_kadaluwarsa = $this->input->post('waktu_kadaluwarsa');
 			$denda = $this->input->post('denda');

 			$data = array(
 				'config_total_pinjam_mahasiswa' => $max_peminjaman,
 				'waktu_kadaluwarsa' =>  $waktu_kadaluwarsa,
 				'nominal_denda' => $denda
 			);

 			$this->db->where('id_config', '1');
 			$this->db->update('config_tables', $data);

			$this->session->set_flashdata('error', "Update pengaturan berhasil");
 			redirect(base_url('operator/pengaturan'));
 		} else {
 			$data = array(
 				'show_current_setting' => $this->db->get('config_tables')->result() 
 			);
	 		$this->load->view('operator/header');
	 		$this->load->view('operator/form/form-update-settings', $data);
 		}
 	}

 } 
 ?>