<?php /**
* 
*/
class Auth extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('auth_model');
	}

	// function index(){
	// 	if ($this->session->userdata('digilib_user_level') == 'Admin') {
	// 		redirect(base_url('admin'))
	// 	} else if ($this->session->userdata('digilib_user_level') == 'Operator') {

	// 	} else {
	// 		redirect(base_url('auth/admin/'));
	// 	}
	// }

	function index() {
		if ($this->session->userdata('digilib_user_level') == 'Admin') {
			redirect(base_url('admin'));
		} else if ($this->session->userdata('digilib_user_level') == 'Operator') {
			redirect(base_url('operator'));
		} else {
			if ($this->input->server('REQUEST_METHOD') == 'POST') {
				$this->auth_model->admin();
			} else {
				$this->load->view('login');
			}
		}
	}

	function logout() {
		if ($this->session->userdata('digilib_user_level') != null) {
			$this->auth_model->logout();
		}
	}
} 
?>