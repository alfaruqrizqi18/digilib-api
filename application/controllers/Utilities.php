<?php /**
* 
*/
class Utilities extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model("util_pemesanan");
		$this->load->model("util_peminjaman");
		$this->load->model("util_notifikasi");
	}

	function pemesanan(){
		$type = $this->input->get("type");

		if ($type == "check_validitas_status") {
			$this->util_pemesanan->check_validitas_status_pemesanan();
		} else {
			echo json_encode(array(
				'Message' => "Periksa kembali type yang diinginkan" 
			)
			);
		}
	}

	function peminjaman(){
		$type = $this->input->get("type");

		if ($type == "check_status_transaksi") {
			$this->util_peminjaman->check_status_transaksi();
		} else {
			echo json_encode(array(
				'Message' => "Periksa kembali type yang diinginkan" 
			)
			);
		}
	}

	function notifikasi(){
		$type = $this->input->get("type");

		if ($type == "1") { // kirim notifikasi sehari sebelum tanggal pengembalian
			$this->util_notifikasi->notifikasi_type_1();
		} elseif ($type == "2" ) { // kirim notifikasi saat tanggal pengembalian
			$this->util_notifikasi->notifikasi_type_2();
		} elseif ($type == "3" ) { // kirim notifikasi apabila melewati tanggal pengembalian
			$this->util_notifikasi->notifikasi_type_3();
		}
	}

} 

?>