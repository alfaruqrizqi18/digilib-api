<?php
header('Content-Type: application/json');
/**
 * 
 */
 class Api extends CI_Controller {
 	
 	function __construct(){
 		parent::__construct();
 		$this->load->model('api_mahasiswa');
 		$this->load->model('api_buku');
 		$this->load->model('api_laporan');
 		$this->load->model('api_pemesanan');
 		$this->load->model('auth_model');
 		$this->load->model('api_peminjaman');
 		$this->load->model('api_notifikasi');
 	}

 	function index(){
 		$this->send_result_if_access_index_function();
 	}

 	function profile(){
 		$nim = $this->input->get('nim');
 		$this->auth_model->profile($nim);
 	}

 	function send_result_if_access_index_function(){
 		$result = array(
 			'status' => 'Hello world!',
 			'message' => 'Apa yang kamu inginkan ?' );
		$this->output->set_output(
			json_encode(
				array('result' => $result)
			)
		);
 	}

 	function auth_mahasiswa(){
 		$method = $this->input->get('method');
 		if ($method === 'post'){
 			if ($this->input->server('REQUEST_METHOD') == 'POST') {
				$this->auth_model->mahasiswa();
			} else {
 				$this->send_result_if_access_index_function();
			}
 		} else {
 			$this->send_result_if_access_index_function();
 		}
 	}

 	function updateTokenAndroid($nim) {
		$this->auth_model->updateToken($nim);
 	}

 	function change_password(){
		$this->auth_model->change_password();
 	}

 	function mahasiswa(){
 		$method = $this->input->get('method');
 		if ($method === 'get') {
 			$this->api_mahasiswa->get_mahasiswa();

 		} else if ($method === 'post'){
 			$this->api_mahasiswa->post_mahasiswa();

 		} else if ($method === 'update') {
 			$this->api_mahasiswa->update_mahasiswa();

 		} else if ($method === 'delete') {
 			$this->api_mahasiswa->delete_mahasiswa();

 		} else {
 			$this->send_result_if_access_index_function();
 		}
 	}

 	function buku(){
 		$method = $this->input->get('method');
 		$id_buku = $this->input->get('id_buku');
 		$judul = $this->input->get('judul');
 		$pengarang = $this->input->get('pengarang');
 		$subyek = $this->input->get('subyek');
 		$isbn = $this->input->get('isbn');
 		$gmd = $this->input->get('gmd');
 		if ($method === 'get' && !$judul) {
 			$this->api_buku->get_all_buku();

 		} else if ($method == 'landingpage') {

 			$this->api_buku->get_buku_for_landingpage_books();

 		} else if ($method == 'landingpage_full_filter') {
 			
 			$this->api_buku->get_landingpage_full_filter($pengarang, $subyek, $isbn, $gmd);

 		} else if ($method === 'get' && $judul && !$pengarang && !$subyek && !$isbn && !$gmd) {

 			$this->api_buku->get_buku_by_param($judul);

 		} elseif ($method === 'full_filter') {

 			$this->api_buku->full_filter($judul, $pengarang, $subyek, $isbn, $gmd);

 		} elseif ($method === 'get_stock' && $id_buku ) {

 			$this->api_buku->get_stock($id_buku);

 		} else {

 			$this->send_result_if_access_index_function();
 		}
 	}


 	function laporan(){
 		$method = $this->input->get('method');
 		$judul = $this->input->get('judul');
 		if ($method === 'get' && !$judul) {
 			$this->api_laporan->get_all_laporan();

 		} else if ($method == 'landingpage') {
 			$this->api_laporan->get_laporan_for_landingpage_laporan();

 		} else if ($method === 'get' && $judul) {
 			$this->api_laporan->get_laporan_by_param($judul);

 		} else {
 			$this->send_result_if_access_index_function();
 		}
 	}

 	function pemesanan(){
 		$method = $this->input->get('method');
 		$id_pemesanan = $this->input->get('id_pemesanan');
 		$status = $this->input->get('status');
 		$nim = $this->input->get('nim');
 		if ($method == 'post') {
 			$this->api_pemesanan->add_pemesanan();
 		} elseif ($method == 'detail') {
 			$this->api_pemesanan->detail_pemesanan($id_pemesanan);
 		} elseif ($method == 'show_pemesanan_by_nim') {
 			$this->api_pemesanan->show_pemesanan_by_nim($nim);
 		} elseif ($method == 'cancel_pemesanan') {
 			$this->api_pemesanan->cancel_pemesanan();
 		} else if($method == 'show_pemesanan_by_nim_filter' ){
 			$this->api_pemesanan->show_pemesanan_by_nim_filter($nim, $status);
 		}
 	}

 	function peminjaman(){
 		$method = $this->input->get('method');
 		$id_transaksi = $this->input->get('id_transaksi');
 		$nim = $this->input->get('nim');
 		$status_transaksi = $this->input->get('status');
 		if ($method == "show_peminjaman_by_nim") {
 			$this->api_peminjaman->show_all_peminjaman_by_nim($nim);
 		} elseif ($method == "detail") {
 			$this->api_peminjaman->detail_peminjaman($id_transaksi);
 		} elseif ($method == 'show_peminjaman_by_nim_filter') {
 			$this->api_peminjaman->show_peminjaman_by_nim_filter($nim, $status_transaksi);
 		}
 	}

 	function notifikasi(){
 		$type = $this->input->get('type');
 		$nim = $this->input->get('nim');

 		if ($type == '1') {
 			$this->api_notifikasi->get_all_notifikasi_type_1_by_nim($nim);
 		}
 	}

 } 
 ?>