<?php 
/**
 * 
 */
 class Admin extends CI_Controller {
 	
 	function __construct(){
 		parent::__construct();
 		if ($this->session->userdata('digilib_user_level') == null) {
 			redirect(base_url('auth'));
 		} else if ($this->session->userdata('digilib_user_level') == 'Operator') {
 			redirect(base_url('operator'));
 		} else {
 			$this->load->model('admin_model');
 		}
 	}

 	function index(){
 		redirect(base_url('admin/data-admin-operator'));
 	}

 	function data_admin_operator(){
 		$data = array(
 			'data_admin' => $this->admin_model->show('adminoperator')
 		);
 		$this->load->view('admin/header');
 		$this->load->view('admin/data-admin-operator', $data);
 	}

 	function data_mahasiswa(){
 		$data = array(
 			'data_mahasiswa' => $this->admin_model->show('mahasiswa')
 		);
 		$this->load->view('admin/header');
 		$this->load->view('admin/data-mahasiswa', $data);
 	}


 	function add($param){
 		if ($param == 'adminoperator') {
	 		if ($this->input->server('REQUEST_METHOD') == 'POST') {
	 			$this->admin_model->add($param);
	 		} else {
	 			$this->load->view('admin/header');
	 			$this->load->view('admin/form/form-add-admin');
	 		}
 		} else if ($param == 'mahasiswa') {
 			if ($this->input->server('REQUEST_METHOD') == 'POST') {
	 			$this->admin_model->add($param);
	 		} else {
	 			$this->load->view('admin/header');
	 			$this->load->view('admin/form/form-add-mhs');
	 		}
 		}
 	}

 	function edit($param, $id){
 		if ($param == 'adminoperator') {
 			if ($this->input->server('REQUEST_METHOD') == 'POST') {
 				$this->admin_model->edit($param, $id);
 			} else {
 				$data = array(
 					'data_admin' => $this->admin_model->show_by_id($param, $id)
 				);
	 			$this->load->view('admin/header');
 				$this->load->view('admin/form/form-edit-admin', $data);
 			}
 		} else if ($param == 'mahasiswa') {
 			if ($this->input->server('REQUEST_METHOD') == 'POST') {
 				$this->admin_model->edit($param, $id);
 			} else {
 				$data = array(
 					'data_mahasiswa' => $this->admin_model->show_by_id($param, $id)
 				);
	 			$this->load->view('admin/header');
 				$this->load->view('admin/form/form-edit-mhs', $data);
 			}
 		}
 	}

 	function delete($param, $id){
 		$this->admin_model->delete($param, $id);
 	}

 } 

 ?>